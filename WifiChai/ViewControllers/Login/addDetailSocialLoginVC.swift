

import UIKit

class addDetailSocialLoginVC: UIViewController {
    
    
    var isUserType : String = ""
    var dictResponse : [String:Any] = [:]
    
    @IBOutlet weak var txtMobileNumber : UITextField!
    
    
    @IBOutlet weak var viewSelectUser : UIView!
    @IBOutlet weak var viewSelectCustmr : UIView!
    
    @IBOutlet weak var btnSelect : UIButton!
    @IBOutlet weak var btnSelectCustmr : UIButton!
    @IBOutlet weak var imgSelectCustmr : UIImageView!
    
    @IBOutlet weak var btnSubmitTop : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSelectUser.isHidden = true
        viewSelectCustmr.isHidden = true
        btnSelect.setTitle("Customer", for: .normal)
        btnSelectCustmr.setTitle("Personal", for: .normal)
        
        isUserType = "2"

        // Do any additional setup after loading the view.
    }

    @IBAction func clickOnSelectBtn(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelectUser.isHidden = false
        })
    }
    
    @IBAction func clickOsnCustomerBtn(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelectUser.isHidden = true
            self.btnSelect.setTitle("Customer", for: .normal)
            
            self.btnSelectCustmr.isHidden = false
            self.imgSelectCustmr.isHidden = false
            
            self.btnSubmitTop.constant=35
            
            
        })
    }
    
    @IBAction func clickOnVenderBtn(_ sender : UIButton){
        
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelectUser.isHidden = true
            self.btnSelect.setTitle("Vendor", for: .normal)
            
            self.isUserType = "1"
            self.btnSelectCustmr.isHidden = true
            self.imgSelectCustmr.isHidden = true
            
            self.btnSubmitTop.constant=0
        })
        
    }
    
    @IBAction func ClickOnSelectSec(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelectCustmr.isHidden = false
        })
    }
    
    @IBAction func clickOnPrsnBtn(_ sender : UIButton){
        
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelectCustmr.isHidden = true
            self.btnSelectCustmr.setTitle("Personal", for: .normal)
            self.isUserType = "2"
            
            // self.viewVendor.isHidden = false
            // self.viewCustomer.isHidden = true
        })
    }
    
    @IBAction func clickOnCorporateBtn(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelectCustmr.isHidden = true
            self.btnSelectCustmr.setTitle("Corporate", for: .normal)
            
            self.isUserType = "3"
            // self.viewVendor.isHidden = false
            // self.viewCustomer.isHidden = true
        })
    }
    
    
    @IBAction func clickOnAddDetailBtn(_ sender : UIButton){
        if((txtMobileNumber.text == "") || (txtMobileNumber.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Mobile number.", controller: self)
            
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            showHud()
            self.callAddDetailApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
        
    }
    
    func callAddDetailApi(){
        
        
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)","mobile":"\(txtMobileNumber.text!)","user_type":"\(self.isUserType)"] as NSDictionary
        
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.add_details_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                
                
                print("dict---\(dict)")
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let responseDict = dict["response"] as! NSDictionary
                        
                        let mobile = responseDict["phone"] as! String
                        let user_type = responseDict["user_type"] as! String
                        
                        self.dictResponse.updateValue(mobile, forKey: "mobile")
                        self.dictResponse.updateValue(user_type, forKey: "user_type")
                        
                        
                        let userDict = self.dictResponse as NSDictionary
                        print(userDict)
                        
                        userDef.set(userDict, forKey: "userInfo")
                        
                        let strMobileVerify = userDict["is_mobile_verified"] as! Bool
                        if(strMobileVerify == false){
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                                
                                let verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyUserViewController") as! VerifyUserViewController
                                verify.strMobileNo = self.txtMobileNumber.text!
                                self.present(verify, animated: true, completion: nil)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            //Navigate to home screen
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
