
import UIKit
import Photos
import SDWebImage

var takePhoto : String = ""


class DocumentsVC: UIViewController {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var viewDocuments : UIView!
    @IBOutlet weak var viewBankDetails : UIView!
    @IBOutlet weak var viewCompanyDetails : UIView!
    
    @IBOutlet weak var lblLineDoc : UILabel!
    @IBOutlet weak var lblLineBank : UILabel!
    @IBOutlet weak var lblLineCompany : UILabel!
    
    @IBOutlet weak var btnDoc : UIButton!
    @IBOutlet weak var btnBank : UIButton!
    @IBOutlet weak var btnCompany : UIButton!
    
    @IBOutlet weak var btnContinue : UIButton!
    var isDocVerified : Bool = false
    
    
    var Pageint : Int = 0

    //Documents outlet
    @IBOutlet weak var tblDocumentUpload : UITableView!
    var arrDocuments_List : [NSDictionary] = []
    var imgPicker = UIImagePickerController()
    
    var chosenImage = UIImage()
    var asset : PHAsset!
    var imgName : String = ""
    var isFilenameEmpty : String = ""
    
    var document_id : String = ""
    
    //Bank Outlets
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtAcctNo : UITextField!
    @IBOutlet weak var txtIfscCode : UITextField!
    @IBOutlet weak var txtBankName : UITextField!
    @IBOutlet weak var txtBranchAdd : UITextField!
    
    //COMPANY Outlets
    @IBOutlet weak var txtCmpnyName : UITextField!
    @IBOutlet weak var txtCmpnyEmployees : UITextField!
    @IBOutlet weak var txtCmpnyGSTno : UITextField!
    @IBOutlet weak var txtCmpnyTinno : UITextField!
    var strCmpnyAge : Bool = false
    var strCmpnyAgreement : Bool = false
    
    @IBOutlet var btnConditions : [UIButton]!
    
    
    
    
    
    override func viewDidLoad() {
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        viewBankDetails.isHidden=true
        lblLineBank.backgroundColor=UIColor.clear
        btnBank.setTitleColor(.black, for: .normal)
        
        viewCompanyDetails.isHidden=true
        lblLineCompany.backgroundColor=UIColor.clear
        btnCompany.setTitleColor(.black, for: .normal)
        
        viewDocuments.tag=0
        viewBankDetails.tag=1
        viewCompanyDetails.tag=2

     
        if(self.isDocVerified==false){
            self.btnContinue.isEnabled=false
            self.btnContinue.isUserInteractionEnabled=false
            self.btnContinue.backgroundColor=UIColor.lightGray
        }else{
            self.btnContinue.isEnabled=true
            self.btnContinue.isUserInteractionEnabled=true
            self.btnContinue.backgroundColor=self.UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
      
        UIApplication.shared.statusBarView?.backgroundColor = .black
        super.viewDidLoad()
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        if (sender.direction == .left) {
            
            if (Pageint < 2) {
                viewMain.slideInFromRight()
               // lblLineDoc.slideInFromLeft()
                Pageint += 1
                self.changeView(tag: Pageint)
            }
            
        }
        
        
        if (sender.direction == .right) {
            
            if (Pageint > 0) {
                viewMain.slideInFromLeft()
               // lblLineBank.slideInFromRight()
                Pageint -= 1
                self.changeView(tag: Pageint)
                
            }
            
        }
    }
    
    
    func changeView(tag : Int)
    {
        if (tag==0){
            viewBankDetails.isHidden=true
            viewDocuments.isHidden=false
            viewCompanyDetails.isHidden=true
            
            lblLineDoc.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            btnDoc.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: .normal)
            
            lblLineBank.backgroundColor=UIColor.clear
            btnBank.setTitleColor(.black, for: .normal)
            
            lblLineCompany.backgroundColor=UIColor.clear
            btnCompany.setTitleColor(.black, for: .normal)
            
        }
        else if (tag==1) {
            viewBankDetails.isHidden=false
            viewDocuments.isHidden=true
            viewCompanyDetails.isHidden=true
            
            lblLineBank.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            btnBank.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: .normal)
            
            lblLineDoc.backgroundColor=UIColor.clear
            btnDoc.setTitleColor(.black, for: .normal)
            
            lblLineCompany.backgroundColor=UIColor.clear
            btnCompany.setTitleColor(.black, for: .normal)
            
            
        }
        else if (tag==2) {
            viewCompanyDetails.isHidden=false
            viewDocuments.isHidden=true
            viewBankDetails.isHidden=true
            
            lblLineCompany.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            btnCompany.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: .normal)
            
            lblLineDoc.backgroundColor=UIColor.clear
            btnDoc.setTitleColor(.black, for: .normal)
            
            lblLineBank.backgroundColor=UIColor.clear
            btnBank.setTitleColor(.black, for: .normal)
            
            
        }
    }
    

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        imgPicker.delegate = self
        
        self.tblDocumentUpload.delegate = self
        self.tblDocumentUpload.dataSource = self
        
        
        self.tblDocumentUpload.tableFooterView = UIView()
        
        DispatchQueue.main.async {
            self.callGetDocumentListApi()
            self.callGetBankDetailApi()
            self.callGetCompanyDetailApi()
        }
    }
    
    //Get document list of user
    func callGetDocumentListApi(){
        
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.document_list_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let responseDict = dict["response"] as! NSDictionary
                        
                        self.arrDocuments_List = responseDict.object(forKey: "document_list") as! [NSDictionary]
                        self.tblDocumentUpload.reloadData()
                        
                        
                        
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    @IBAction func closeAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension DocumentsVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDocuments_List.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblDocumentUpload.dequeueReusableCell(withIdentifier: "DocumentTableCell") as! DocumentTableCell
        
        let dict = arrDocuments_List[indexPath.section]
        cell.lblDocumentName.text = dict["document_name"] as? String
        document_id = dict["document_id"] as! String
        
        let document_status = dict["document_status"] as! String
        if(document_status=="1"){
            cell.lblDocStatus.text="Add File"
        }else if(document_status=="2"){
            cell.lblDocStatus.text="Pending"
        }else if(document_status=="3"){
            cell.lblDocStatus.text="Verified"
            cell.lblDocStatus.textColor=UIColor.green
        }else if(document_status=="4"){
            cell.lblDocStatus.text="Rejected"
        }
        
        
        let doc_url = dict["document_url"] as? String
        if(doc_url==""){
            cell.btnUpload.setImage(UIImage(named: "upload"), for: .normal)
            cell.imgDocuments.image=UIImage(named: "gallery_placeholder")
        }
        else{
            
            cell.btnUpload.setImage(UIImage(named: "editFile"), for: .normal)
            
            cell.imgDocuments.setIndicatorStyle(.gray)
            cell.imgDocuments.setShowActivityIndicator(true)
            
            if let imageURL = URL(string:"\(doc_url!)") {
                cell.imgDocuments.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
            }
        }
        
        cell.btnUpload.addTarget(self, action: #selector(self.callImgUploadApi(sender:)), for: .touchUpInside)
        cell.btnUpload.tag=indexPath.section
        
        
        return cell
        
        
    }
    
    @objc func callImgUploadApi(sender: UIButton){
        let buttonTag = sender.tag
        ApiResponse.CameraGallery(controller: self, imagePicker: imgPicker)
    }
    
    func saveImg(chooseImg : UIImage){
        
        showHud()
        
        let url = NSURL(string: Constant.uploadDocument_URL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var body = Data()
        
        var imgfieName : String = ""
        if (takePhoto == "Gallery") {
            if(asset==nil){
                imgfieName="FirstImg.png"
            }
            else{
                let fname = (asset.value(forKey: "filename"))!   //"test.png"
                
                let words = (fname as! String).components(separatedBy: ".")
                let fnm=words[0]
                
                imgfieName="\(fnm).png"
            }
            
        }
        else {
            imgfieName="\(imgName).png"
        }
        
        print(imgfieName)
        let mimetype = "image/png"
        
        
        
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let parameters : [String : String] = ["user_id" : "\(userid)","document_id":"\(document_id)"]
        print(parameters)
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        
        let image_data = UIImageJPEGRepresentation(chooseImg, 0.5)
        //let image_data = UIImageJPEGRepresentation(chosenImage, 0.5)
        if(image_data == nil)
        {
            return
        }
        //define the data post parameter
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"document_image\"; filename=\"\(imgfieName)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            
            
            
            
            DispatchQueue.main.async {
                self.hideHUD()
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                
                let dd = try! JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                print("Profile img Response : \(dd)")
                let status = dd["status"] as! Bool
                let message = dd["message"] as! String
                if (status == false) {
                    ApiResponse.alert(title: "", message: message, controller: self)
                }
                else {
                    ApiResponse.alert(title: "", message: message, controller: self)
                    
                    self.document_id=""
                }
            }
        }
        task.resume()
    }
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

class DocumentTableCell: UITableViewCell {
    
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var lblDocStatus: UILabel!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var imgDocuments : UIImageView!
}

extension DocumentsVC :  UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        
        isFilenameEmpty="chooseImg"
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //imgView.contentMode = .scaleAspectFit //3
        // imgView.image = chosenImage //4
        if(takePhoto == "Camera"){
            
            let dict = userDef.value(forKey: "userInfo") as! NSDictionary
            let userFname = (dict.object(forKey: "first_name") as! String)
            //let userLname=(dict.object(forKey: "last_name") as! String)
            
            let first2 = userFname.substring(to:userFname.index(userFname.startIndex, offsetBy: 2))
            let userId=(dict.object(forKey: "id") as! String)
            
            let i = userDef.object(forKey: "index")
            if(i==nil)
            {
                imgName = "\(first2)\(userId)0"
                userDef.set("1", forKey: "index")
            }
            else{
                imgName = "\(first2)\(userId)\(String(describing: i!))"
                
                if var zz = i as? Int{
                    zz += 1
                    userDef.set(zz, forKey: "index")
                    print(userDef.object(forKey: "index")!)
                    //Do your stuff
                }
                else if var zz = i as? String{
                    if var xx = Int(zz){
                    }
                }
            }
        }
        else if(takePhoto == "Gallery"){
            if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                asset = result.firstObject
            }
        }
        
        self.saveImg(chooseImg: chosenImage)
        
        dismiss(animated:true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension DocumentsVC {
    
    
    //Get bank details of user
    func callGetBankDetailApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.getBankDetails_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        let responseDict = dict["response"] as! NSDictionary
                        
                        let bankDetails = responseDict["bank_details"] as! NSDictionary
                        self.txtName.text = bankDetails["account_holder_name"] as? String
                        
                        self.txtAcctNo.text = bankDetails["account_number"] as? String
                        self.txtBankName.text = bankDetails["bank_name"] as? String
                        self.txtBranchAdd.text = bankDetails["branch_address"] as? String
                        self.txtIfscCode.text = bankDetails["ifsc_code"] as? String
                        self.txtName.text = bankDetails["account_holder_name"] as? String
                        
                        self.isDocVerified = responseDict["is_document_verified"] as! Bool
                        if(self.isDocVerified==false){
                            self.btnContinue.isEnabled=false
                            self.btnContinue.isUserInteractionEnabled=false
                            self.btnContinue.backgroundColor=UIColor.lightGray
                        }else{
                            self.btnContinue.isEnabled=true
                            self.btnContinue.isUserInteractionEnabled=true
                            self.btnContinue.backgroundColor=self.UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
                        }
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    
    //Send bank details
    @IBAction func clickOnSubmitBtn(_ sender : UIButton){
        
        if((txtName.text == "") || (txtName.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Account holder name.", controller: self)
            
        }
        else if((txtAcctNo.text == "") || (txtAcctNo.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Account number.", controller: self)
        }
        else if((txtIfscCode.text == "") || (txtIfscCode.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter IFSC code.", controller: self)
        }
        else if((txtBankName.text == "") || (txtBankName.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Bank Name.", controller: self)
        }
        else if((txtBranchAdd.text == "") || (txtBranchAdd.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Branch Address.", controller: self)
        }
            
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            self.callSetBankDetailsApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
        
    }
    
    func callSetBankDetailsApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)","account_number":"\(txtAcctNo.text!)","account_holder_name":"\(txtName.text!)","ifsc_code":"\(txtIfscCode.text!)","bank_name":"\(txtBankName.text!)","branch_address":"\(txtBranchAdd.text!)"] as NSDictionary
        
        
        ApiResponse.onResponsePost(url: Constant.setBankDetails_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
}

extension DocumentsVC {
    
    //Get bank details of user
    func callGetCompanyDetailApi(){
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.getCompanyDetails_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        
                        let bankDetails = responseDict["company_detail"] as! NSDictionary
                        self.txtCmpnyName.text = bankDetails["company_name"] as? String
                        
                        self.txtCmpnyEmployees.text = bankDetails["employee_count"] as? String
                        self.txtCmpnyGSTno.text = bankDetails["gst_number"] as? String
                        self.txtCmpnyTinno.text = bankDetails["tin_number"] as? String
                        
//                        let employee_count = bankDetails["employee_count"] as? Int
//                        let is_age_valid = bankDetails["is_age_valid"] as? Bool
//                        let is_agreement_selected = bankDetails["is_agreement_selected"] as? Bool
//                        
//                        print(employee_count)
//                        print(is_age_valid)
//                        print(is_agreement_selected)
                        
                        //       self.strCmpnyAge = (bankDetails["is_age_valid"] as? Bool)!
                        //      self.strCmpnyAgreement = (bankDetails["is_agreement_selected"] as? Bool)!
                        self.isDocVerified = responseDict["is_document_verified"] as! Bool
                        if(self.isDocVerified==false){
                            self.btnContinue.isEnabled=false
                            self.btnContinue.isUserInteractionEnabled=false
                            self.btnContinue.backgroundColor=UIColor.lightGray
                        }else{
                            self.btnContinue.isEnabled=true
                            self.btnContinue.isUserInteractionEnabled=true
                            self.btnContinue.backgroundColor=self.UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
                        }
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        
                        
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    
    //Send bank details
    @IBAction func clickOnSubmitCompanyBtn(_ sender : UIButton){
        
        if((txtCmpnyName.text == "") || (txtCmpnyName.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter name.", controller: self)
            
        }
        else if((txtCmpnyEmployees.text == "") || (txtCmpnyEmployees.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter total number of employees.", controller: self)
        }
        else if((txtCmpnyGSTno.text == "") || (txtCmpnyGSTno.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter GST number.", controller: self)
        }
        else if((txtCmpnyTinno.text == "") || (txtCmpnyTinno.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Tin number.", controller: self)
        }
        else if strCmpnyAgreement == false {
            ApiResponse.alert(title: "", message: "Please select terms and condition", controller: self)
        }
            
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            self.callSetCompanyDetailsApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
        
    }
    
    func callSetCompanyDetailsApi(){
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)","company_name":"\(txtCmpnyName.text!)","employee_count":"\(txtCmpnyEmployees.text!)","is_age_valid":"\(strCmpnyAge)","is_agreement_selected":"\(strCmpnyAgreement)","gst_number":"\(txtCmpnyGSTno.text!)","tin_number":"\(txtCmpnyTinno.text!)"] as NSDictionary
        
        
        
        ApiResponse.onResponsePost(url: Constant.setCompanyDetails_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    @IBAction func clickTermBtn(_ sender : UIButton){
        
        let Selectedtag = sender.tag
        for button in self.btnConditions {
            let tag = button.tag
            if Selectedtag == tag{
                button.setImage(UIImage(named : "checkMArk"), for: .normal)
                strCmpnyAgreement=true
            }else{
                button.setImage(UIImage(named : "unCheckMark"), for: .normal)
                strCmpnyAgreement=false
            }
        }
    }
}
