
import UIKit
import GooglePlacePicker
import CoreLocation

class ConfirmOrderVC: UIViewController , CLLocationManagerDelegate {
    
    @IBOutlet weak var lblAddressPick : UILabel!
    
    var merchant:PGMerchantConfiguration!
    
    
    @IBOutlet weak var btnSubmit : UIButton!
    @IBOutlet weak var btnCod : UIButton!
    @IBOutlet weak var btnPaytm : UIButton!
    
    var payment_type : String = ""
    
    var datePicker = UIDatePicker()
    @IBOutlet weak var heightlayoutContarint : NSLayoutConstraint!
    @IBOutlet weak var btnSelect : UIButton!
    
    @IBOutlet weak var contentViewHeight : NSLayoutConstraint!
    
    var dictPlaceOrderDetails : NSDictionary!
    
    var isSelectPicker : String = ""
    var isSelectPaymentMode : String = ""
    
    var StrCurrentDate : String = ""
    var StrCurrentTime : String = ""
    var isWallet_select : Bool = false
    
    
    var locationManager:CLLocationManager!
    var lat : Double = 0.0
    var long : Double = 0.0
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate lazy var timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()
    
    @IBOutlet weak var txtDate : UITextField!
    @IBOutlet weak var txtTime : UITextField!
    
    
    @IBOutlet weak var vendorName : UILabel!
    @IBOutlet weak var vendorMobileno : UILabel!
    @IBOutlet weak var vendorAddress : UILabel!
    @IBOutlet weak var vendorProfileImg : UIImageView!
    //@IBOutlet weak var btnTime : UILabel!
    
    
    @IBOutlet weak var lblCount : UILabel!
    @IBOutlet weak var lblRate : UILabel!
    @IBOutlet weak var lblTax : UILabel!
    @IBOutlet weak var lblTotalAmt : UILabel!
    @IBOutlet weak var lblPayAmt : UILabel!
    
    
    @IBOutlet weak var lblWalletAmount : UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        btnSubmit.setTitle("Confirm Order", for: .normal)
        isSelectPaymentMode="cod"
        
        // print(dictPlaceOrderDetails)
        
        let prodct_id = dictPlaceOrderDetails["product_id"] as! String
        if(prodct_id=="1"){
            lblCount.text="Tea Count: \(dictPlaceOrderDetails["quantity"] as! String)"
            lblRate.text = "Rate: INR \(dictPlaceOrderDetails["product_rate"] as! String)/Tea"
        }else if(prodct_id=="2"){
            lblCount.text="Coffee Count: \(dictPlaceOrderDetails["quantity"] as! String)"
            lblRate.text = "Rate: INR \(dictPlaceOrderDetails["product_rate"] as! String)/Coffee"
        }
        
        lblTax.text = "Tax: \(dictPlaceOrderDetails["tax"] as! String) %"
        
        lblTotalAmt.text = "Total Amount: INR \(dictPlaceOrderDetails["total_amount"] as! String)"
        lblPayAmt.text = "Payable Amount : INR \(dictPlaceOrderDetails["total_amount"] as! String)"
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        lblWalletAmount.text = "INR \(Userdict["wallet_amount"] as! String)"
        
        
        let vendorDict=dictPlaceOrderDetails["vendor"] as! NSDictionary
        vendorName.text=vendorDict["name"] as? String
        vendorMobileno.text=vendorDict["mobile"] as? String
        vendorAddress.text=vendorDict["address"] as? String
        
        let profile_pic_url = vendorDict["profile_pic"] as? String
        if(profile_pic_url==""){
            vendorProfileImg.image=UIImage(named: "gallery_placeholder")
        }
        else{
            vendorProfileImg.setIndicatorStyle(.gray)
            vendorProfileImg.setShowActivityIndicator(true)

            if let imageURL = URL(string:"\(profile_pic_url!)") {
                vendorProfileImg.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
            }
        }
        
        
        self.determineMyCurrentLocation()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.clickOnLabelAddress(_:)))
        
        lblAddressPick.addGestureRecognizer(tap)
        lblAddressPick.isUserInteractionEnabled = true
        
        btnSelect.isSelected=true
        
        let todaysDate = Date()
        datePicker.minimumDate = todaysDate
        
        
        
        heightlayoutContarint.constant=32
        
        self.createToolBarOnDatePicker(textfield: txtDate)
        self.createToolBarOnDatePicker(textfield: txtTime)
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        locationManager.startUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        lat=userLocation.coordinate.latitude
        long=userLocation.coordinate.longitude
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error.localizedDescription)")
    }
    
    
    
    // function which is triggered when handleTap is called
    func clickOnLabelAddress(_ sender: UITapGestureRecognizer) {
        let center = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        self.present(placePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MArk advance booking Code
    @IBAction func clickOnCheckBtn(_ sender : UIButton){
        if (sender.isSelected == true)
        {
            sender.setBackgroundImage(UIImage(named: "checkMArk"), for: UIControlState.normal)
            heightlayoutContarint.constant=75
            self.contentViewHeight.constant=heightlayoutContarint.constant + 671
            sender.isSelected = false
            
        }
        else
        {
            sender.setBackgroundImage(UIImage(named: "unCheckMark"), for: UIControlState.normal)
            heightlayoutContarint.constant=32
            sender.isSelected = true
            
            let todayDate : String = ""
            StrCurrentDate = todayDate.getCurrentDate()
            let todayTime : String = ""
            StrCurrentTime = todayTime.getTodayTime()
            
            txtDate.text=""
            txtTime.text=""
        }
    }
    
    
    @IBAction func clickOnSelectWallet(_ sender : UIButton){
        
        if (sender.isSelected == true)
        {
            sender.setBackgroundImage(UIImage(named: "checkMArk"), for: UIControlState.normal)
            isWallet_select=true
            sender.isSelected = false
            
        }
        else
        {
            sender.setBackgroundImage(UIImage(named: "unCheckMark"), for: UIControlState.normal)
            isWallet_select=false
            sender.isSelected = true
        }
    }
    
  
}


extension ConfirmOrderVC : GMSPlacePickerViewControllerDelegate{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        self.lblAddressPick.text=place.formattedAddress!
        self.contentViewHeight.constant=self.lblAddressPick.frame.height + 671 + 50
        
        
        lat=place.coordinate.latitude
        long=place.coordinate.longitude
        
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        // print("Place name \(place.name)")
        //print("Place address \(String(describing: place.formattedAddress))")
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
}

//date and time select

extension ConfirmOrderVC : UITextFieldDelegate{
    
    func createToolBarOnDatePicker(textfield : UITextField){
        
        let toolBar = UIToolbar(frame: CGRect(x:0, y:self.view.frame.size.height/6, width:self.view.frame.size.width, height:40.0))
        
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        
        toolBar.barStyle = .blackTranslucent
        
        toolBar.tintColor = UIColor.white
        
        toolBar.backgroundColor = UIColor.black
        
        
        
        //Aligh Left
        let cancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(clickOnCancelBtn(_:)))
        
        let done = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(clickOnDoneBtn(_:)))
        
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        toolBar.setItems([cancel,flexSpace,flexSpace,done], animated: true)
        textfield.inputAccessoryView = toolBar
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField==txtDate){
            
            isSelectPicker="Date"
            datePicker.datePickerMode = .date
            txtDate.inputView = datePicker
            
            
        }else if(textField==txtTime){
            
            isSelectPicker="Time"
            datePicker.datePickerMode = .time
            txtTime.inputView = datePicker
        }
        
    }
    
    
    
    
    func clickOnDoneBtn(_ sender : UIButton){
        
        if(isSelectPicker=="Date"){
            
            let strDate = dateFormatter.string(from: datePicker.date)
            txtDate.text=strDate
            StrCurrentDate=txtDate.text!
            self.view.endEditing(true)
            
        }else if(isSelectPicker=="Time"){
            
            let strDate = timeFormatter.string(from: datePicker.date)
            txtTime.text=strDate
            StrCurrentTime = txtTime.text!
            self.view.endEditing(true)
        }
        
    }
    
    func clickOnCancelBtn(_ sender : UIButton){
        txtDate.resignFirstResponder()
        txtTime.resignFirstResponder()
    }
}

//Paytm integration
extension ConfirmOrderVC{
    
    @IBAction func clickOnCodBtn(_ sender : UIButton){
        
        if(sender.tag==0){
            
            isSelectPaymentMode="cod"
            payment_type="1"
            
            btnSubmit.setTitle("Confirm Order", for: .normal)
            btnCod.setImage(UIImage(named: "checkRadio"), for: .normal)
            
            btnPaytm.setImage(UIImage(named: "unChechRadio"), for: .normal)
            
            btnSubmit.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        
            
        }else if(sender.tag==1){
            isSelectPaymentMode="paytm"
            payment_type="2"
            btnSubmit.setTitle("Pay with Paytm", for: .normal)
            btnPaytm.setImage(UIImage(named: "checkRadio"), for: .normal)
            btnCod.setImage(UIImage(named: "unChechRadio"), for: .normal)
            btnSubmit.backgroundColor=UIColorFromHex(rgbValue: 0x03b8f3, alpha: 1.0)
            
        }
        
    }
    
    
    @IBAction func clickOnConfirmOrderBtn(_ sender : UIButton){
        if(isSelectPaymentMode=="cod"){
            
            if((lblAddressPick.text == "Click to select delivery address")){
                ApiResponse.alert(title: "Alert", message: "Please select Delivery Address.", controller: self)
                
            }else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
            {
                showHud()
                self.callMAkePaymentApi(strTransId: "", strTxnStatus: "")
            }
            else{
                ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
            }
            
        }else if(isSelectPaymentMode=="paytm"){
            setMerchant()
        }
    }
    
    
    //MAke payment using COD option
    func callMAkePaymentApi(strTransId : String , strTxnStatus : String){
        
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        
        let orderId = dictPlaceOrderDetails["order_id"] as? String
        
        var param = NSDictionary()
        
        if(StrCurrentDate==""){
            let todayDate : String = ""
            StrCurrentDate = todayDate.getCurrentDate()
        }
        if(StrCurrentTime==""){
            let todayTime : String = ""
            StrCurrentTime = todayTime.getTodayTime()
            
        }
        
        if(payment_type=="1"){
            param = ["user_id" : "\(userid)","order_id":"\(orderId!)","payment_type":"1","use_wallet":"\(isWallet_select)","address":"\(lblAddressPick.text!)","latitude":"\(lat)","longitude":"\(long)","date":"\(StrCurrentDate)","time":"\(StrCurrentTime)"] as NSDictionary
        }else{
            param = ["user_id" : "\(userid)","order_id":"\(orderId!)","payment_type":"1","use_wallet":"\(isWallet_select)","address":"\(lblAddressPick.text!)","latitude":"\(lat)","longitude":"\(long)","date":"\(StrCurrentDate)","time":"\(StrCurrentTime)","transaction_id":"\(strTransId)","status":"\(strTxnStatus)"] as NSDictionary
        }
        
    
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.COD_make_payment_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        
                        let alert = UIAlertController(title: "", message: "Order placed successfully.", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
        
    }
    
    
    
    
    
    
    
    
    func getPaytmCheckSum(){
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        
        let orderId = dictPlaceOrderDetails["order_id"] as? String
        let totalAmount = dictPlaceOrderDetails["total_amount"] as? String
        
        let param = ["user_id" : "\(userid)","order_id":"\(orderId!)","total_amount":"\(totalAmount!)"] as NSDictionary
        
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.get_checksum_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    
                OperationQueue.main.addOperation {
                    
                    self.hideHUD()
                    
                    let responseDict = dict["response"] as! NSDictionary
                    print(responseDict)
                    
                    let callbackurl = self.dictPlaceOrderDetails["callback_url"] as? String
                    let checksumHash = responseDict["checksum"] as? String
                    
                    self.merchant  = PGMerchantConfiguration.default()!
                    
                     var odrDict = [String : String]()
                    odrDict["MID"] = "\(self.merchant.merchantID!)"
                    odrDict["WEBSITE"] = "\(self.merchant.website!)"
                    odrDict["INDUSTRY_TYPE_ID"] = "\(self.merchant.industryID!)"
                    odrDict["CHANNEL_ID"] = "\(self.merchant.channelID!)"
                    odrDict["TXN_AMOUNT"] = "\(totalAmount!)"
                    odrDict["ORDER_ID"] = "\(orderId!)"
                    odrDict["CALLBACK_URL"] = "\(callbackurl!)"
                    odrDict["CUST_ID"] = "\(userid)"
                    odrDict["CHECKSUMHASH"] = "\(checksumHash!)"
                    
                    print(odrDict)
                    
                    let order: PGOrder = PGOrder(params: odrDict)
                  
                    
                        let transaction = PGTransactionViewController.init(transactionFor: order)
                        print("transaction == \(transaction!)")
                        transaction!.serverType = eServerTypeProduction
                        transaction!.merchant = self.merchant
                        transaction!.delegate = self
                        self.present(transaction!, animated: true, completion: {
                            
                        })
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            }
        }
        
    }
    
    
    
    func setMerchant(){
        merchant = PGMerchantConfiguration.default()!
        merchant.merchantID = "CragFO64001029199264";//paste here your merchant id  //mandatory
        merchant.website = "CragFOWAP";//mandatory
        merchant.industryID = "Retail109";//mandatory
        merchant.channelID = "WAP"; //provided by PG WAP //mandatory
        
        self.getPaytmCheckSum()
    }
    
    
    
    func randomStringWithLength(len: Int) -> NSString {
        
        let letters : NSString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 1...len{
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
}

extension ConfirmOrderVC : PGTransactionDelegate{
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        print("cancel")
    }
    
    func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        print(response)
    }
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        
        print(responseString)
        let dict = convertToDictionary(text: responseString)
        print(dict!)
        
        //        let str = "{\"ORDERID\":\"162\", \"MID\":\"CragFO64001029199264\", \"TXNID\":\"20180814111212800110168416933582411\", \"TXNAMOUNT\":\"1.00\", \"PAYMENTMODE\":\"PPI\", \"CURRENCY\":\"INR\", \"TXNDATE\":\"2018-08-14 18:40:01.0\", \"STATUS\":\"TXN_SUCCESS\", \"RESPCODE\":\"01\", \"RESPMSG\":\"Txn Success\", \"GATEWAYNAME\":\"WALLET\", \"BANKTXNID\":\"91516965238\", \"BANKNAME\":\"WALLET\", \"CHECKSUMHASH\":\"j3HQeM8D1ZjeSAkFZdXoCQQyFZxIEc8lD8YFIAcsem9ISlsfI3t+hQQbA0FZiQ/ktXcTIvR+5H4lsPq90XiqtzE3mBN0pkKKoHtBwZa8EII=\"}"
        
        
        let rescode = dict!["RESPCODE"] as! String
        let status = dict!["STATUS"] as! String
        
        let transID = dict!["TXNID"] as! String
       // let orderID = dict!["ORDERID"] as! String
     //   let txnAmount = dict!["TXNAMOUNT"] as! String
        
        let orderStatus : String!
        if(rescode=="01" && status=="TXN_SUCCESS"){
            orderStatus="true"
        }else{
            orderStatus="false"
        }
        
        
        self.callMAkePaymentApi(strTransId: transID, strTxnStatus: orderStatus)
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
   
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        print("error--\(error)")
    }
    
    func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        print(response)
    }
    
    
    func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        print(error)
    }
    
    
    
    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        print("response == \(response!)")
    }
    
    
    
}
