

import UIKit

class CorporateCustHomeVC: BaseViewController {
    
    @IBOutlet weak var tblCorporateBooking : UITableView!
    var arrBookingList : NSMutableArray = []
    var reversedArr : [NSDictionary] = []
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var btnRequest : UIButton!
    
    var cofeePriceCurrent : String = ""
    var teaPriceCurrent : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dict = userDef.value(forKey: "userInfo") as! NSDictionary
        let usertype=dict["user_type"] as! String
        userDef.set(usertype, forKey: "userType")
        
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let docVerified = Userdict["is_document_verified"] as! Bool
        if(docVerified==true){
            btnRequest.isEnabled=true
            self.btnRequest.backgroundColor=self.UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            
        }else{
            btnRequest.isEnabled=false
            self.btnRequest.backgroundColor=UIColor.lightGray
        }
        

        tblCorporateBooking.tableFooterView=UIView()
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        tblCorporateBooking.isHidden=true
        self.lblStatus.isHidden=true
        
        DispatchQueue.main.async {
            self.callBookingListApi()
        }
        
        self.hideKeyboardWhenTappedAround() 
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    @IBAction func clickOnSideMenu(_ sender : UIButton){
        onSlideMenuButtonPressed(sender)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func callBookingListApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.corporate_booking_list_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
              
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        
                        let res = dict["response"] as! NSDictionary
                        self.cofeePriceCurrent=(res["coffee_rate"] as? String)!
                        self.teaPriceCurrent=(res["tea_rate"] as? String)!
                        
                        if let result = res["booking_list"] as? NSArray {
                            for i in 0..<(result.count){
                                if let dict = result[i] as? NSDictionary {
                                    self.arrBookingList.add(dict)
                                }
                            }
                            self.reversedArr = self.arrBookingList.reversed() as! [NSDictionary]
                            if(self.reversedArr.count==0){
                                self.lblStatus.isHidden=false
                                self.tblCorporateBooking.isHidden=true
                                
                            }else{
                                self.lblStatus.isHidden=true
                                self.tblCorporateBooking.isHidden=false
                                self.tblCorporateBooking.reloadData()
                            }
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    @IBAction func clickOnRequestBookingBtn(_ sender : UIButton){
        
        let vendorStory = AppStoryboard.Main.instance
        let modalViewController = vendorStory.instantiateViewController(withIdentifier: "requestBookingVC") as! requestBookingVC
        let cPrice = Int(cofeePriceCurrent)
        let tPrice = Int(teaPriceCurrent)
        modalViewController.cofeePrice=cPrice!
        modalViewController.teaPrice=tPrice!
        self.present(modalViewController, animated: true, completion: nil)
    }
}

extension CorporateCustHomeVC : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return reversedArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCorporateBooking.dequeueReusableCell(withIdentifier: "custTableBookingTblCell") as! custTableBookingTblCell
        let dict = reversedArr[indexPath.section]
        cell.lblBookingId.text = "Booking Id : \((dict["booking_id"] as? String)!)"
        let dateAsString = "\((dict["end_date"] as? String)!)"
        let strr : String = ""
        cell.lblToDate.text = "To: \(strr.changeDateFormat(strDate:dateAsString))"
        
        
        let fromDate = "\((dict["start_date"] as? String)!)"
        let strrr : String = ""
        cell.lblFromDate.text = "From: \(strrr.changeDateFormat(strDate:fromDate))"
            
        let slotArr = dict["list_of_slots"] as! [NSDictionary]
        cell.lblSlotsCount.text = "Number of slots : \(slotArr.count)"
        
        cell.btnDEtailView.tag=indexPath.section
        cell.btnDEtailView.addTarget(self, action: #selector(clickOnDetailBtn(_:)), for: .touchUpInside)
        cell.btnDEtailView.addTopBorderWithColor(color: .lightGray, width: 1.5)

        return cell
        
    }
    
    func clickOnDetailBtn(_ sender : UIButton){
        let dict = reversedArr[sender.tag]
        let vendorStory = AppStoryboard.Main.instance
        let modalViewController = vendorStory.instantiateViewController(withIdentifier: "corporateBookingDetailVC") as! corporateBookingDetailVC
        modalViewController.dictDetails=dict
        self.present(modalViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
    }
}

class custTableBookingTblCell: UITableViewCell {
    @IBOutlet weak var lblBookingId : UILabel!
    @IBOutlet weak var lblToDate : UILabel!
    @IBOutlet weak var lblFromDate : UILabel!
    @IBOutlet weak var lblSlotsCount : UILabel!
    
    @IBOutlet weak var btnDEtailView : UIButton!
}
