

import UIKit
import CoreLocation
import MapKit

class VendorHomeVC: vendorBaseViewController , UISearchBarDelegate {
    
    @IBOutlet weak var tblHomeVendor : UITableView!
    var arrVendorDetail : [NSDictionary] = []
    
    @IBOutlet weak var searchHomeBar : UISearchBar!
    var filtered : [NSDictionary] = []
    var searchActive : Bool = false
    
    @IBOutlet weak var lblCupsSold : UILabel!
    @IBOutlet weak var lblPaytmCash : UILabel!
    @IBOutlet weak var lblCod : UILabel!
    
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var DeliverPinView : UIView!
    @IBOutlet weak var txtPin : UITextField!
    
    var dictDataDeliver : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dict = userDef.value(forKey: "userInfo") as! NSDictionary
        let usertype=dict["user_type"] as! String
        userDef.set(usertype, forKey: "userType")
        
        self.lblStatus.isHidden=true
        
        tblHomeVendor.tableFooterView=UIView()
        DispatchQueue.main.async {
            self.callHomeVendorDetailApi()
        }
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        ApiResponse.decorateTextField(txtPin, keyBoardType: .numberPad, placeHolder: "code", isSecureText: false)
        
        
        DeliverPinView.isHidden=true
        
        
       
        
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
//    override func viewWillLayoutSubviews() {
//        DispatchQueue.main.async {
//            var fram = self.tblHomeVendor.frame
//            //This code will run in the main thread:
//            fram.size.height = self.tblHomeVendor.contentSize.height;
//            self.tblHomeVendor.frame = fram;
//        }
//    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchBar.text?.isEmpty)!{
            searchActive = false
            tblHomeVendor.reloadData()
        } else {
            
            searchActive = true
            filtered.removeAll()
            
            for i in 0...arrVendorDetail.count-1{
                
                let dict = arrVendorDetail[i]
                let vendorDict = dict["customer"] as! NSDictionary
                let tmp = vendorDict["name"] as! NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                
                if(range.location != NSNotFound){
                    filtered.append(dict)
                }else{
                }
            }
        }
        tblHomeVendor.reloadData()
    }
    
    
    func callHomeVendorDetailApi(){
        
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.vendor_home_detail_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let responseDict = dict["response"] as! NSDictionary
                        let prodDict = responseDict["product"] as! NSDictionary
                        
                        self.lblCod.text="\((prodDict["total_cod"] as? NSNumber)!)"
                        self.lblPaytmCash.text="\((prodDict["total_paytm"] as? NSNumber)!)"
                        self.lblCupsSold.text="\((prodDict["total_product"] as? NSNumber)!)"
                        
                        
                        let arrVndr = responseDict.object(forKey: "new_order_list") as! [NSDictionary]
                        self.arrVendorDetail = arrVndr.reversed()
                        
                        
                        
                        if(self.arrVendorDetail.count>0){
                            self.lblStatus.isHidden=true
                            self.tblHomeVendor.isHidden=false
                            
                            
                            self.tblHomeVendor.reloadData()
                        }else{
                            self.lblStatus.isHidden=false
                            self.tblHomeVendor.isHidden=true
                        }
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                
                OperationQueue.main.addOperation {
                    self.hideHUD()
                    
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                    
                }
                
                
            }
        }
    }
    
    func openDeliverPinView(dict : NSDictionary){
        DeliverPinView.isHidden=false
        dictDataDeliver=dict
    }
    
    @IBAction func clickOnCancelViewBtn(_ sender : UIButton){
        DeliverPinView.isHidden=true
    }
    
    @IBAction func clickOnConfrmViewBtn(_ sender : UIButton){
        showHud()
        let orderID = (dictDataDeliver["order_id"] as? String)!
        let orderStatusType = dictDataDeliver["order_statustype"] as! NSNumber
        let param = ["order_id" : "\(orderID)" , "pin":"\(txtPin.text!)" ,"order_statustype":orderStatusType] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.order_deliver_vendor_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                            self.viewDidLoad()
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                
                OperationQueue.main.addOperation {
                    self.hideHUD()
                    
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            }
        }
    }
    
    
    @IBAction func cllickOnMEnuBtn(_ sender : UIButton){
        onSlideVendorMenuButtonPressed(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension VendorHomeVC : UITableViewDelegate , UITableViewDataSource , UIGestureRecognizerDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return arrVendorDetail.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblHomeVendor.dequeueReusableCell(withIdentifier: "vendorHomeTableCell") as! vendorHomeTableCell
        
        var dict : NSDictionary!
        if(searchActive){
            dict = filtered[indexPath.section]
        }else{
            dict = arrVendorDetail[indexPath.section]
        }
        
        
        let orderStatusType = dict["order_statustype"] as! NSNumber
        if(orderStatusType==1){
            
            cell.lblCorporate_OrderId.isHidden=true
            cell.lblCoffeeCount.isHidden=true
            cell.btnCancel.isHidden=false
            cell.btnDelivered.isHidden=false
            cell.custViewHeight.constant=270
            cell.btnDeliverTrailling.constant = 20
            
            cell.lblCod.isHidden=false
            cell.lblTotalAmt.isHidden=false
            cell.lblVendorEarning.isHidden=false
            cell.lblAmount.isHidden=false
            
            cell.lblOrder_BookId.text="  Order Id : \((dict["order_id"] as? String)!)"
            let productId = dict["product_id"] as! String
            let quant = dict["quantity"] as! String
            if(productId=="1"){
                cell.lblTeaCount.text = "  No of Tea : \(quant)"
            }else if(productId=="2"){
                cell.lblTeaCount.text = "  No of Coffee : \(quant)"
            }
            
            cell.lblCustAddresst.text="\((dict["address"] as? String)!)"
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
            cell.lblCustAddresst.isUserInteractionEnabled = true
            tap.delegate=self
            cell.lblCustAddresst.addGestureRecognizer(tap)
            
            
            
            
            cell.lblTotalAmt.text="  Total Amount : INR \((dict["total_amount"] as? String)!)"
            
            
            let dateAsString = "\((dict["date"] as? String)!)"
            let strr : String = ""
            cell.lblDate.text = "  Date: \(strr.changeDateFormat(strDate:dateAsString))"
            
            let timeAsString = "\((dict["time"] as? String)!)"
            let strTime : String = ""
            cell.lblTime.text = "  Time :\(strTime.changeTimeFormat(strDate: timeAsString))"
            
            
            let payment_type = dict["payment_type"] as! String
            if(payment_type=="1"){
                cell.lblCod.text = "  Cash on Delivery"
                cell.btnCancel.isHidden=false
                
            }else if(payment_type=="2"){
                cell.lblCod.text = "  Paid Online"
                cell.btnCancel.isHidden=true
            }
            
            
            
            if let amt = dict["product_rate"] as? String{
                let amount : Int? = Int(amt)
                let quantity : Int? = Int(quant)   //
                let calculateAmt = Int(amount! * quantity!)
                
                let tax = dict["tax"] as! String
                cell.lblAmount.text="  Amount : INR : \(calculateAmt) Tax:\(tax)%"
            }
            else{
                let tax = dict["tax"] as! String
                cell.lblAmount.text="  Amount : INR :0.00    Tax:\(tax)%"
            }
            
            cell.lblVendorEarning.text="  Vendor Earning : \((dict["vendor_earning"] as? String)!)"
            
            cell.lblFlag.text="  Today Order"
            cell.lblFlag.backgroundColor=UIColorFromHex(rgbValue: 0x42a64d, alpha: 1.0)
            cell.lblFlag.setRoundedCorners([.topRight ,.bottomRight], radius: 15.0)
            
            cell.btnCancel.tag=indexPath.section
            cell.btnCancel.addTarget(self, action: #selector(clickOnCancelBtn(_:)), for: .touchUpInside)

            cell.btnDelivered.tag=indexPath.section+100
            cell.btnDelivered.addTarget(self, action: #selector(clickOnDeliveredBtn(_:)), for: .touchUpInside)
            
            
            
        } else if (orderStatusType==2){
            
            cell.lblCorporate_OrderId.isHidden=true
            cell.lblCoffeeCount.isHidden=true
            cell.btnCancel.isHidden=true
            cell.btnDelivered.isHidden=true
            cell.custViewHeight.constant=225
            cell.btnDeliverTrailling.constant = 20
            
            cell.lblCod.isHidden=false
            cell.lblTotalAmt.isHidden=false
            cell.lblVendorEarning.isHidden=false
            cell.lblAmount.isHidden=false
            
            
            cell.lblOrder_BookId.text="  Order Id : \((dict["order_id"] as? String)!)"
            let productId = dict["product_id"] as! String
            let quant = dict["quantity"] as! String
            if(productId=="1"){
                cell.lblTeaCount.text = "  No of Tea : \(quant)"
            }else if(productId=="2"){
                cell.lblTeaCount.text = "  No of Coffee : \(quant)"
            }
            
            cell.lblCustAddresst.text="\((dict["address"] as? String)!)"
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
            cell.lblCustAddresst.isUserInteractionEnabled = true
            tap.delegate=self
            cell.lblCustAddresst.addGestureRecognizer(tap)
            
            cell.lblTotalAmt.text="  Total Amount : INR \((dict["total_amount"] as? String)!)"
            
            
            let payment_type = dict["payment_type"] as! String
            if(payment_type=="1"){
                cell.lblCod.text = "  Cash on Delivery"
            }else if(payment_type=="2"){
                cell.lblCod.text = "  Paid Online"
            }
            
            if let amt = dict["product_rate"] as? String{
                let amount : Int? = Int(amt)
                let quantity : Int? = Int(quant)   //
                let calculateAmt = Int(amount! * quantity!)
                
                let tax = dict["tax"] as! String
                cell.lblAmount.text="  Amount : INR : \(calculateAmt)      Tax:\(tax)%"
            }
            cell.lblVendorEarning.text="Vendor Earning : \((dict["vendor_earning"] as? String)!)"
            //cell.lblVendorEarning.text="  Vendor Earning : 0"
            let dateAsString = "\((dict["date"] as? String)!)"
            let strr : String = ""
            cell.lblDate.text = "  Date: \(strr.changeDateFormat(strDate:dateAsString))"
            
            let timeAsString = "\((dict["time"] as? String)!)"
            let strTime : String = ""
            cell.lblTime.text = "  Time :\(strTime.changeTimeFormat(strDate: timeAsString))"
            
            
            cell.lblFlag.text=" Advance Order"
            cell.lblFlag.backgroundColor=UIColorFromHex(rgbValue: 0xf57025, alpha: 1.0)
            cell.lblFlag.setRoundedCorners([.topRight ,.bottomRight], radius: 15.0)
            
            

        }
        else if (orderStatusType==3){
            
            cell.lblCorporate_OrderId.isHidden=false
            cell.lblCod.isHidden=true
            cell.lblTotalAmt.isHidden=true
            cell.lblVendorEarning.isHidden=true
            cell.lblCoffeeCount.isHidden=false
            cell.btnCancel.isHidden=true
            cell.btnDelivered.isHidden=false
            cell.lblAmount.isHidden=true
            cell.custViewHeight.constant=235
            
            
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    cell.btnDeliverTrailling.constant = -70
                case 1334:
                    cell.btnDeliverTrailling.constant = -50
                case 2208:
                    cell.btnDeliverTrailling.constant = -20
                case 2436:
                    cell.btnDeliverTrailling.constant = -20
                default:
                    print("unknown")
                }
            }
            
            
            cell.lblFlag.text="  Corporate Order"
            cell.lblFlag.backgroundColor=UIColorFromHex(rgbValue: 0x268fc7, alpha: 1.0)
            cell.lblFlag.setRoundedCorners([.topRight ,.bottomRight], radius: 15.0)
            
            cell.lblOrder_BookId.text="  Booking Id : \((dict["booking_id"] as? String)!)"
            cell.lblCorporate_OrderId.text="  Order Id : \((dict["booking_id"] as? String)!)"
            cell.lblTeaCount.text = "  No of Tea : \((dict["no_of_tea"] as? String)!)"
            cell.lblCoffeeCount.text = "  No of Coffee : \((dict["no_of_coffee"] as? String)!)"
            
          //  cell.lblAmount.text="  Tax:\((dict["tax"] as? String)!)%"
            
            cell.lblCustAddresst.text="\((dict["address"] as? String)!)"
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
            cell.lblCustAddresst.isUserInteractionEnabled = true
            tap.delegate=self
            cell.lblCustAddresst.addGestureRecognizer(tap)
            
            let dateAsString = "\((dict["date"] as? String)!)"
            let strr : String = ""
            cell.lblDate.text = "  Date: \(strr.changeDateFormat(strDate:dateAsString))"
            
            let timeAsString = "\((dict["time"] as? String)!)"
            let strTime : String = ""
            cell.lblTime.text = "  Time :\(strTime.changeTimeFormat(strDate: timeAsString))"
            
            cell.btnDelivered.tag=indexPath.section+100
            cell.btnDelivered.addTarget(self, action: #selector(clickOnDeliveredBtn(_:)), for: .touchUpInside)
            
            
        }
        
        
        let custDetail = dict["customer"] as! NSDictionary
        cell.lblName.text="\((custDetail["name"] as? String)!)"
        cell.lblNumber.text="\((custDetail["mobile"] as? String)!)"
        
        
        let status = dict["status"] as! String
        if(status=="1"){
            cell.lblOrderStatus.text = "Initialized"
            
        }else if(status=="2"){
            cell.lblOrderStatus.text = "Pending"
            
        }else if(status=="3"){
            cell.lblOrderStatus.text = "Delivered"
        }
        else if(status=="4"){
            cell.lblOrderStatus.text = "Cancelled"
            cell.lblOrderStatus.textColor = UIColor.red
        }
        
        return cell
    }
    
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        
        UIView.animate(withDuration: 0.5, animations: {
            
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
            
        }, completion: nil)
        
    }
    

   
    
    
    func clickOnCancelBtn(_ sender : UIButton){
        let alert = UIAlertController(title: "Cancel Order", message: "Are you sure you want to cancel this order?", preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
            
            self.showHud()
            
            let dict = self.arrVendorDetail[sender.tag]
            let orderID = (dict["order_id"] as? String)!
            let payment_type = dict["payment_type"] as! String
            
            let param = ["order_id" : "\(orderID)" , "payment_type":"\(payment_type)"] as NSDictionary
            
            ApiResponse.onResponsePost(url: Constant.order_cancel_vendor_URL, parms: param) { (dict, error) in
                if(error == ""){
                    let strMsg=dict["message"] as! String
                    let status=dict["status"] as! Bool
                    if (status==true){
                        OperationQueue.main.addOperation {
                            self.hideHUD()
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                                self.viewDidLoad()
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            self.hideHUD()
                            ApiResponse.alert(title: "", message: strMsg, controller: self)
                        }
                    }
                }
                else{
                    
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        if (error == Constant.Status_Not_200) {
                            ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                        }
                        else {
                            ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                        }
                    }
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action) in
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    
    func clickOnDeliveredBtn(_ sender : UIButton){
        let dict = arrVendorDetail[sender.tag-100]
        self.openDeliverPinView(dict: dict)
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        let searchlbl:UILabel = (sender.view as! UILabel)
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(searchlbl.text!) { (placemarksOptional, error) -> Void in
            if let placemarks = placemarksOptional {
                if let location = placemarks.first?.location {
                    let query = "?ll=\(location.coordinate.latitude),\(location.coordinate.longitude)"
                   _ = "http://maps.apple.com/" + query
                    
                    let coordinates = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
                    let regionSpan =   MKCoordinateRegionMakeWithDistance(coordinates, 1000, 1000)
                    let placemark = MKPlacemark(coordinate: location.coordinate, addressDictionary: nil)
                    let mapItem = MKMapItem(placemark: placemark)
                    mapItem.openInMaps(launchOptions:[
                        MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center)
                        ] as [String : Any])
                    
//                    if let url =  NSURL(string: path) {
//                       // UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
//                    //    let request:MKDirectionsRequest = MKDirectionsRequest()
//                     //   mapView.addOverlay(route.polyline, level: MKOverlayLevel.AboveRoads)
//                    } else {
//                        // Could not construct url. Handle error.
//                    }
                } else {
                    // Could not get a location from the geocode request. Handle error.
                }
            } else {
                // Didn't get any placemarks. Handle error.
            }
        }
        
    }
}

class vendorHomeTableCell : UITableViewCell{
    
    @IBOutlet weak var lblOrder_BookId : UILabel!
    @IBOutlet weak var lblCorporate_OrderId : UILabel!
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblCoffeeCount : UILabel!
    @IBOutlet weak var lblCustAddresst : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblTotalAmt : UILabel!
    @IBOutlet weak var lblCod : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblNumber : UILabel!
    @IBOutlet weak var lblVendorEarning : UILabel!
    
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    
    @IBOutlet weak var lblFlag : UILabel!
    @IBOutlet weak var lblOrderStatus : UILabel!
    
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnDelivered : UIButton!
    
    @IBOutlet weak var stackview : UIStackView!
    @IBOutlet weak var content_View : UIView!
    
    
    @IBOutlet weak var btnDeliverTrailling : NSLayoutConstraint!
    @IBOutlet weak var custViewHeight : NSLayoutConstraint!
}

