
import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController {
    
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        
       //
        // Do any additional setup after loading the view.
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        
        let usertype = userDef.value(forKey: "userType") as! String
        
        if(usertype=="2"){
            arrayMenuOptions.append(["title":"Home", "icon":"Home"])
            arrayMenuOptions.append(["title":"Orders", "icon":"Order"])
            arrayMenuOptions.append(["title":"Notification", "icon":"notification"])
            arrayMenuOptions.append(["title":"Profile", "icon":"profileIcon"])
            arrayMenuOptions.append(["title":"Settings", "icon":"setting"])
            arrayMenuOptions.append(["title":"Refer & Earn", "icon":"Refer-&-earn"])
            arrayMenuOptions.append(["title":"Logout", "icon":"Logout"])
        }
        else if(usertype=="3"){
            arrayMenuOptions.append(["title":"Home", "icon":"Home"])
            arrayMenuOptions.append(["title":"Orders", "icon":"Order"])
            arrayMenuOptions.append(["title":"Notification", "icon":"notification"])
            arrayMenuOptions.append(["title":"Profile", "icon":"profileIcon"])
            arrayMenuOptions.append(["title":"Settings", "icon":"setting"])
            arrayMenuOptions.append(["title":"Refer & Earn", "icon":"Refer-&-earn"])
            arrayMenuOptions.append(["title":"Logout", "icon":"Logout"])
        }
        
       
        
        tblMenuOptions.reloadData()
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
    
   
}
extension MenuViewController :  UITableViewDataSource, UITableViewDelegate{
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableCell") as! menuTableCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        
        
        cell.imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        cell.lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}

class menuTableCell : UITableViewCell{
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgIcon : UIImageView!
}
