
import UIKit
import CoreLocation

struct Product {
    var value = 0
}

protocol CartSelection {
    func addProductToCart(product : Product, atindex : Int , tag : Int)
    func removeProductFromCart(product : Product, atindex : Int , tag : Int)
}


class VendorStoreVC: vendorBaseViewController , CartSelection , CLLocationManagerDelegate{
    
    @IBOutlet weak var lblLineStore : UILabel!
    @IBOutlet weak var lblLineOrderHistory : UILabel!
    
    @IBOutlet weak var viewHeader : UIView!
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var viewStore : UIView!
    @IBOutlet weak var viewOrderHistory : UIView!
    
    
    
    var didFindLocation : Bool = false
    
    @IBOutlet weak var btnStore : UIButton!
    @IBOutlet weak var btnOrderHistory : UIButton!
    
    @IBOutlet weak var tblStoreDetails : UITableView!
    @IBOutlet weak var tblOrderHistory : UITableView!
    
    
    
    var grandTotal : Int = 0
    var totalQuant : Int = 0
    var item_id : String = ""
   
     var arrItem : NSMutableArray = []
    var arrStoreData : NSMutableArray = []
    var arrOrderHistoty : NSMutableArray = []
    var arrReveresed : [NSDictionary] = []
    
    
    var productArray = [Product]()

    
     var Pageint : Int = 0
    var value : Int = 0
    
     @IBOutlet weak var lblTotalAmnt : UILabel!
     @IBOutlet weak var btnConfirm : UIButton!
    
    
    @IBOutlet weak var lblLocationStatus : UILabel!
    var locationManager:CLLocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        
     //   btnStore.titleLabel?.layer.shouldRasterize = true
      //  btnStore.titleLabel?.layer.shadowRadius = 0.5
     //   btnStore.titleLabel?.layer.shadowOpacity = 0.5
      //  btnStore.titleLabel?.shadowOffset = CGSize(width: 1, height: 1)
      //  btnStore.setTitleShadowColor(UIColor.lightGray, for: .normal)
        
      //  btnOrderHistory.titleLabel?.layer.shouldRasterize = true
      //  btnOrderHistory.titleLabel?.layer.shadowRadius = 0.5
      // btnOrderHistory.titleLabel?.layer.shadowOpacity = 0.5
       // btnOrderHistory.titleLabel?.shadowOffset = CGSize(width: 1, height: 1)
      //  btnOrderHistory.setTitleShadowColor(UIColor.lightGray, for: .normal)
        
        
        viewOrderHistory.isHidden=true
        lblLineOrderHistory.backgroundColor=UIColor.clear
        btnOrderHistory.setTitleColor(.black, for: .normal)
        
        tblStoreDetails.tableFooterView=UIView()
        tblOrderHistory.tableFooterView=UIView()
        
        
         self.determineMyCurrentLocation()
        
         self.lblLocationStatus.isHidden=true
        self.viewMain.isHidden=true
        self.viewHeader.isHidden=true
        lblTotalAmnt.isHidden=true
        btnConfirm.isHidden=true
        
        UIApplication.shared.statusBarView?.backgroundColor = .black

        // Do any additional setup after loading the view.
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func determineMyCurrentLocation() {
        
       //showHud()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        
       // if(userLocation.coordinate.latitude == 23.0225 && userLocation.coordinate.longitude == 72.5714){
        
        if(didFindLocation==false){
            DispatchQueue.main.async {
                
                self.hideHUD()
                self.lblLocationStatus.isHidden=true
                self.viewMain.isHidden=false
                self.viewHeader.isHidden=false
                self.callGetStoreItemListApi()
                self.callStoreOrderHistoryApi()
                
                self.didFindLocation=true
                self.locationManager.stopUpdatingLocation()
            }
        }
        
        
//        }
//        else{
//            OperationQueue.main.addOperation {
//                self.hideHUD()
//                self.lblLocationStatus.isHidden=false
//                self.viewMain.isHidden=true
//                 self.viewHeader.isHidden=true
//            }
//
//        }
        
        
        
        
        
    }
    
    
    @IBAction func cllickOnMEnuBtn(_ sender : UIButton){
        onSlideVendorMenuButtonPressed(sender)
    }
    
    
    @IBAction func clickOnDetailSwipeRightBtn(_ sender : UIButton){
        if (Pageint < 1) {
            viewMain.slideInFromRight()
            lblLineStore.slideInFromLeft()
            Pageint += 1
            self.changeView()
        }
    }
    
    @IBAction func clickOnStoreBtn(_ sender : UIButton){
        if (Pageint > 0) {
            viewMain.slideInFromLeft()
            lblLineOrderHistory.slideInFromRight()
            Pageint -= 1
            self.changeView()
            
        }
        
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer){
        
        if (sender.direction == .left) {
            
            if (Pageint < 1) {
                viewMain.slideInFromRight()
                lblLineStore.slideInFromLeft()
                Pageint += 1
                self.changeView()
            }
           
        }
        
        
        if (sender.direction == .right) {
            
            if (Pageint > 0) {
                viewMain.slideInFromLeft()
                lblLineOrderHistory.slideInFromRight()
                Pageint -= 1
                self.changeView()
                
            }
         
        }
    }
    
    
    func changeView(){
        if (viewOrderHistory.isHidden==true) {
            viewOrderHistory.isHidden=false
            viewStore.isHidden=true
            
            lblLineOrderHistory.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            btnOrderHistory.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: .normal)
            
            lblLineStore.backgroundColor=UIColor.clear
            btnStore.setTitleColor(.black, for: .normal)
            
            
            
            
        }
        else if (viewStore.isHidden==true){
            viewOrderHistory.isHidden=true
            viewStore.isHidden=false
            
            lblLineStore.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            btnStore.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: .normal)
            
            lblLineOrderHistory.backgroundColor=UIColor.clear
            btnOrderHistory.setTitleColor(.black, for: .normal)
        }
    }
    
    
    func callGetStoreItemListApi(){
        
        self.arrStoreData.removeAllObjects()
        showHud()
        ApiResponse.onResponseGet(url: "\(Constant.get_vendor_storeitem_list_URL)") { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        if let res = dict["response"] as? NSArray {
                            for i in 0..<(res.count){
                                if let dict = res[i] as? NSDictionary {
                                    self.arrStoreData.add(dict)
                                    self.productArray.append(Product(value: 0))
                                }
                            }
                            self.tblStoreDetails.reloadData()
                        }
                        
            
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    func callStoreOrderHistoryApi(){
        showHud()
        self.arrOrderHistoty.removeAllObjects()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.storeorder_history_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                let arrStoreHistoryData : NSMutableArray = []
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        if let res = dict["response"] as? NSArray {
                            
                            
                            for i in 0..<(res.count){
                                if let dict = res[i] as? NSDictionary {
                                    self.arrOrderHistoty.add(dict)
                                   // arrStoreHistoryData.add(dict)
                                }
                            }
                            
                            self.arrReveresed = self.arrOrderHistoty.reversed() as! [NSDictionary]
                            
                            
                            
                            
                            self.tblOrderHistory.reloadData()
                        }
                        
                        
                        //                        if(self.arrVendorDetail.count>0){
                        //                            self.lblStatus.isHidden=true
                        //                            self.tblHomeVendor.isHidden=false
                        //
                        //                            self.tblHomeVendor.reloadData()
                        //                        }else{
                        //                            self.lblStatus.isHidden=false
                        //                            self.tblHomeVendor.isHidden=true
                        //                        }
                        
                        
                        
                        
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                
                OperationQueue.main.addOperation {
                    self.hideHUD()
                    
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                    
                }
                
                
            }
        }
    }
    
    @IBAction func clickOnConfirmOrder(_ sender : UIButton){
        //self.callPlaceStoreOrderApi()
        self.test()
    }
    
    func callPlaceStoreOrderApi(){
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["vendor_id" : "\(userid)","item_list" : self.arrItem] as NSDictionary
        ApiResponse.onResponsePost(url: Constant.placestore_order_URL, parms: param) { (dict, error) in
        if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        let res = dict["response"] as? [NSDictionary]
                        let alert = UIAlertController(title: "", message: "Your order placed successfull", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                            let vendorStory = AppStoryboard.Vendor.instance
                            let home = vendorStory.instantiateViewController(withIdentifier: "VendorHomeVC") as! VendorHomeVC
                            self.present(home, animated: true, completion: nil)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension VendorStoreVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var count:Int?
        if(tableView == self.tblStoreDetails){
            count = arrStoreData.count
        }
        
        if(tableView == self.tblOrderHistory){
            count = arrReveresed.count
        }
        
        return count!
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = UITableViewCell()
        
        if(tableView == self.tblStoreDetails){
          
            let cell = tblStoreDetails.dequeueReusableCell(withIdentifier: "storeTableCell") as! storeTableCell
            let dict = arrStoreData.object(at: indexPath.section) as! NSDictionary
            cell.lblProductName.text=dict["name"] as? String
            cell.lblPrice.text="INR: \((dict["price"] as? String)!)"
            
            cell.product = productArray[indexPath.section]
            cell.lblQuant.text="\(cell.product.value)"
            cell.cartSelectionDelegate = self
            
            cell.btnAdd.tag=indexPath.section
            cell.btnMinus.tag=indexPath.section
            
            return cell
        }
        
        if(tableView == self.tblOrderHistory){
            
            let cell = tblOrderHistory.dequeueReusableCell(withIdentifier: "orderHistryTableCell") as! orderHistryTableCell
            
            let dict = arrReveresed[indexPath.section]
            let arrItem = dict["item_list"] as! [NSDictionary]
            let arrcount = arrItem.count
            cell.lblItemCount.text="Items : \(arrcount)"
            let deliv = (dict["delivery_code"] as! NSString).intValue
            cell.lblDeliveryCode.text="Delivery Code : \(deliv)"
            cell.lblOrderId.text="Order Id : \((dict["order_id"] as! NSString).intValue)"
            
            cell.lblAmount.text="Amount : \((dict["order_total"] as! NSNumber).stringValue)"
            
            let stats = (dict["order_status"] as! NSString).intValue
            if(stats == 1){
                cell.lblStatus.text="Pending"
                cell.lblStatus.textColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
                
            }
            else if(stats == 2){
                cell.lblStatus.text="Delivered"
                cell.lblStatus.textColor=UIColor.green
            }
            
            
            cell.btnDetail.tag=indexPath.section
            cell.btnDetail.addTarget(self, action: #selector(clickOnDetailBtn(_:)), for: .touchUpInside)
            
            return cell
        }
        return cell
        
    }
    
    @objc func clickOnDetailBtn(_ sender : UIButton){
        
        let dict = arrReveresed[sender.tag]
        let arrItem = dict["item_list"] as! [NSDictionary]
        let modalViewController = self.storyboard?.instantiateViewController(withIdentifier: "VendorOrderDetailVC") as! VendorOrderDetailVC
        modalViewController.arrItems_List=arrItem
        modalViewController.modalPresentationStyle = .overCurrentContext
        self.present(modalViewController, animated: true, completion: nil)
    }
    
    func addProductToCart(product: Product, atindex: Int, tag: Int) {
        productArray[atindex] = product
        calculateTotal(index: tag)
    }
    
    func removeProductFromCart(product: Product, atindex: Int, tag: Int) {
        productArray[atindex] = product
        if(product.value==0){
            
        }else{
            calculateMinusTotal(index: tag)
        }
        
    }
    
    func calculateMinusTotal(index : Int){
        var totalValue = 0
        for objProduct in productArray {
            
            
            totalValue += objProduct.value
            let dict = arrStoreData[index] as! NSDictionary
            let price = dict["price"] as? String
            let priceToInt = Int(price!)
            if(grandTotal==0){
            }else{
                grandTotal = (grandTotal) - (priceToInt)!
            }
            break
        }
        
        if(grandTotal==0){
            lblTotalAmnt.isHidden=true
            btnConfirm.isHidden=true
            
        }else{
            self.lblTotalAmnt.text="Total Amount : INR\(grandTotal)"
            lblTotalAmnt.isHidden=false
            btnConfirm.isHidden=false
        }
    }
  
    func test() {
        for i in 0...arrStoreData.count - 1 {
            let dict =  arrStoreData[i] as! NSDictionary

            let item_id = dict["item_id"] as! String
            let indexpath = IndexPath.init(row: 0, section: i)
            let cell = tblStoreDetails.cellForRow(at: indexpath) as! storeTableCell
            
            let quantitiy = cell.lblQuant.text
            let dictItem = ["item_id":"\(item_id)" , "quantity":"\(quantitiy!)"]
            
            if quantitiy != "0" {
            arrItem.add(dictItem)
            }
           
        }
        self.callPlaceStoreOrderApi()
    }
    
    func calculateTotal(index : Int){
        var totalValue = 0
       
        for objProduct in productArray {
            totalValue += objProduct.value
            let dict = arrStoreData[index] as! NSDictionary
            let price = dict["price"] as? String
            let priceToInt = Int(price!)
            grandTotal = (grandTotal) + (priceToInt)!
           
            break
          }
        
        self.lblTotalAmnt.text="Total Amount : INR\(grandTotal)"
        lblTotalAmnt.isHidden=false
        btnConfirm.isHidden=false
    }
    
    
   
}

class storeTableCell: UITableViewCell {
    
    @IBOutlet weak var lblProductName : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblQuant : UILabel!
    @IBOutlet weak var btnAdd : UIButton!
    @IBOutlet weak var btnMinus : UIButton!
    
    var product : Product!
    private var counterValue = 0
    var productIndex = 0
    
    var cartSelectionDelegate: CartSelection?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func minusButton(_ sender: UIButton) {
       
            cartSelectionDelegate?.removeProductFromCart(product: product, atindex: productIndex, tag: sender.tag)
        
        if(counterValue != 0){
            counterValue -= 1;
        }
        self.lblQuant.text = "\(counterValue)"
        // if(counterValue != 0){
        product.value = counterValue
      // }
       
    }
    @IBAction func plusButton(_ sender: UIButton){
        counterValue += 1;
        self.lblQuant.text = "\(counterValue)"
        product.value = counterValue
        cartSelectionDelegate?.addProductToCart(product: product, atindex: productIndex, tag: sender.tag)
        
    }
    
}

class orderHistryTableCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblItemCount : UILabel!
    @IBOutlet weak var lblDeliveryCode : UILabel!
    @IBOutlet weak var btnDetail : UIButton!
    
}



