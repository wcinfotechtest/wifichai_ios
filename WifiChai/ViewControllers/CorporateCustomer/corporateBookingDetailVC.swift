import UIKit
import SDWebImage
class corporateBookingDetailVC: UIViewController {

    @IBOutlet weak var tblDetailBooking : UITableView!
    
    var dictDetails : NSDictionary = [:]
    var slotList : NSMutableArray = []
    
    @IBOutlet weak var lblBookingId : UILabel!
    @IBOutlet weak var lblStartDate : UILabel!
    @IBOutlet weak var lblEndDate : UILabel!
    @IBOutlet weak var lblSlotCount : UILabel!
    @IBOutlet weak var lblTotalAmt : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var lblVendorAssignStatus : UILabel!
    
    @IBOutlet weak var imgCustomer : UIImageView!
    @IBOutlet weak var lblCustomerName : UILabel!
    @IBOutlet weak var lblCustomerNumber : UILabel!
    @IBOutlet weak var lblCustomerAddress : UILabel!
    @IBOutlet var ratingCustomer : [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(dictDetails)
        
        lblBookingId.text = "Booking Id : \((dictDetails["booking_id"] as? String)!)"
        
        let dateAsString = "\((dictDetails["end_date"] as? String)!)"
        let strr : String = ""
        lblEndDate.text = "End Date: \(strr.changeDateFormat(strDate:dateAsString))"
        
        let booking_status = (dictDetails["booking_status"] as? String)!
        if(booking_status=="1"){
            lblStatus.text="Initialised"
        }else if(booking_status=="2"){
            lblStatus.text="Assing"
        }else if(booking_status=="3"){
            lblStatus.text="Accept"
        }else if(booking_status=="4"){
            lblStatus.text="Deny"
        }else if(booking_status=="5"){
            lblStatus.text="Cancel"
        }
        
        let fromDate = dictDetails["start_date"] as! String
        let strrr : String = ""
        lblStartDate.text = "Start Date: \(strrr.changeDateFormat(strDate:fromDate))"
        lblTotalAmt.text = "Total Amount: \((dictDetails["total_amount"] as? Int)!)"
        
        let slotArr = dictDetails["list_of_slots"] as! [NSDictionary]
        lblSlotCount.text = "Number of slots : \(slotArr.count)"
        
        let arr = dictDetails["list_of_slots"] as! NSArray
        for i in 0..<(arr.count){
            if let dict = arr[i] as? NSDictionary {
                self.slotList.add(dict)
            }
        }
        
        let dictCustmr = dictDetails["vendor_details"] as! NSDictionary
        if (dictCustmr.count>0){
            lblVendorAssignStatus.isHidden=true
            lblCustomerName.isHidden=false
            lblCustomerNumber.isHidden=false
            lblCustomerAddress.isHidden=false
            imgCustomer.isHidden=false
            
            for image in ratingCustomer {
                image.isHidden = false
            }
            
            let img=dictCustmr["profile_pic"] as? String
            imgCustomer.setIndicatorStyle(.gray)
            imgCustomer.setShowActivityIndicator(true)
            if let imageURL = URL(string:"\(img!)") {
                imgCustomer.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
            }else{
                imgCustomer.image=UIImage(named: "placeholder")
            }
            
            lblCustomerName.text=dictCustmr["name"] as? String
            lblCustomerNumber.text=dictCustmr["phone"] as? String
            lblCustomerAddress.text=dictCustmr["address"] as? String
            
            let rating=dictCustmr["ratings"] as! NSNumber
            let myInt = (rating).doubleValue
            ApiResponse.callRating(myInt, starRating: ratingCustomer)
        }
        else{
            lblVendorAssignStatus.isHidden=false
            lblCustomerName.isHidden=true
            lblCustomerNumber.isHidden=true
            lblCustomerAddress.isHidden=true
            imgCustomer.isHidden=true
            
            for image in ratingCustomer {
                image.isHidden = true
            }
        }
    
        
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clikcONBackbtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnDownloadInvoiceBtn(_ sender : UIButton){
        showHud()
        let invoiceUrl = dictDetails["invoice_url"] as! String
        print(invoiceUrl)
        if(invoiceUrl==""){
            OperationQueue.main.addOperation {
                self.hideHUD()
                ApiResponse.alert(title: "", message: "Error in downloading invoice.", controller: self)
            }
            
        }else{
            // Create destination URL
            let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
            let destinationFileUrl = documentsUrl.appendingPathComponent("downloadedFile.pdf")
            
            //Create URL to the source file you want to download
            let fileURL = URL(string: "\(invoiceUrl)")
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        OperationQueue.main.addOperation {
                            self.hideHUD()
                            
                            let alert = UIAlertController(title: "Alert", message: "Successfully downloaded.", preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                                
                                let web = self.storyboard?.instantiateViewController(withIdentifier: "PdfFileViewController") as! PdfFileViewController
                                self.present(web, animated: true, completion: nil)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        print("already dowlod")
                    } catch ( _ ) {
                        
                        
                    }
                    
                } else {
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "");
                }
            }
            task.resume()
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

   

}


extension corporateBookingDetailVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return slotList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblDetailBooking.dequeueReusableCell(withIdentifier: "corporateDetailBookTblCell") as! corporateDetailBookTblCell
        let dict = self.slotList[indexPath.section] as! NSDictionary
        cell.lblCofeeCount.text = "Coffee Count: \((dict["no_of_coffee"] as? String)!)"
        cell.lblTeaCount.text = "Tea Count : \((dict["no_of_tea"] as? String)!)"
       // cell.lblTime.text = "Time : \((dict["time"] as? String)!)"
        
        let timeAsString = "\((dict["time"] as? String)!)"
        let strr : String = ""
        cell.lblTime.text = "Time: \(strr.changeTimeFormatWidoutSec(strDate:timeAsString))"
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(section==0){
            let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            
            let headerLabel = UILabel(frame: CGRect(x: 8, y: 8, width:
                tableView.bounds.size.width, height: 19.5))
            headerLabel.font = UIFont.boldSystemFont(ofSize: 16)
            headerLabel.textColor = UIColor.black
            headerLabel.text = "SLOT LIST"
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            
            return headerView
            
        }else{
            return nil
        }
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section==0){
             return 28
        }else{
             return 0
        }
       
    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if(section==0){
//             return 0
//        }
//        else{
//            return 300
//        }
//    }
}

class corporateDetailBookTblCell : UITableViewCell{
    
    @IBOutlet weak var lblCofeeCount : UILabel!
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblTime : UILabel!
}
