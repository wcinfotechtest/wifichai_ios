//
//  VerifyUserViewController.swift
//  WifiChai
//
//  Created by MacbookPro on 09/06/18.
//  Copyright © 2018 mavencluster. All rights reserved.
//

import UIKit

class VerifyUserViewController: UIViewController {
    
    var strMobileNo : String = ""
    @IBOutlet weak var lblMobileNo : UILabel!
    
    @IBOutlet weak var txtOtp1 : UITextField!
    @IBOutlet weak var txtOtp2 : UITextField!
    @IBOutlet weak var txtOtp3 : UITextField!
    @IBOutlet weak var txtOtp4 : UITextField!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblMobileNo.text = "PLEASE TYPE VERIFICATION CODE SENT TO  \(strMobileNo)"
        
        
        txtOtp1.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtOtp2.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtOtp3.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtOtp4.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)

        // Do any additional setup after loading the view.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField){
        
        let text = textField.text
        
        if text?.count == 1{
            switch textField{
            case txtOtp1:
                txtOtp2.becomeFirstResponder()
            case txtOtp2:
                txtOtp3.becomeFirstResponder()
            case txtOtp3:
                txtOtp4.becomeFirstResponder()
            case txtOtp4:
                txtOtp4.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    
    
    @IBAction func clickOnSubmitBtn(_ sender : UIButton){
        
        let txtOtp = "\(txtOtp1.text!)\(txtOtp2.text!)\(txtOtp3.text!)\(txtOtp4.text!)"
        
        if((txtOtp == "") || (txtOtp.count)==1){
            ApiResponse.alert(title: "Alert", message: "Please enter OTP.", controller: self)
            
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            self.callVerifyUser()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    //Mark : Verify Otp
    func callVerifyUser(){
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let txtOtp = "\(txtOtp1.text!)\(txtOtp2.text!)\(txtOtp3.text!)\(txtOtp4.text!)"
        
        
        let param = ["user_id" : "\(userid)","otp" : "\(txtOtp)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.verifyOTP_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        
                        let strDocVerified = Userdict["is_document_verified"] as! Bool
                        let usertype=dict["user_type"] as! String
                        if(usertype=="2"){
                            userDef.set("login", forKey: "session")
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            self.present(home, animated: true, completion: nil)
                        }
                        else if(usertype=="3"){
                            if(strDocVerified == false){
                                let alert = UIAlertController(title: "", message: "Verify your documents", preferredStyle: UIAlertControllerStyle.alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                                    
                                    let verify = self.storyboard?.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
                                    self.present(verify, animated: true, completion: nil)
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            else{
                                
                                userDef.set("login", forKey: "session")
                                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                self.present(home, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }

    }
    
    @IBAction func clickOnResendOtp(_ sender : UIButton){
        self.callResendOtpApi()
        
    }

    //Mark : Resend Otp
    func callResendOtpApi(){
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let user_Mobileno = Userdict["mobile"] as! String
        let param = ["mobile" : "\(user_Mobileno)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.sendOTP_URL, parms: param) { (dict, error) in
            if(error == ""){
                print(dict)
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark : verfify user api
    func callVErifyOtpApi(){
        
    }
    
}

extension VerifyUserViewController : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    
}
