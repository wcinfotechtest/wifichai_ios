import UIKit
import CoreLocation

class customerOrderHistryVC: BaseViewController , UISearchBarDelegate{

    @IBOutlet weak var tblCorporateOrderHistory : UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive : Bool = false
    
    var arrOrderList : [NSDictionary] = []
    var filtered : [NSDictionary] = []
    @IBOutlet weak var lblStatus : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.callOrderHistoryListApi()
        }
        
        
        tblCorporateOrderHistory.isHidden=true
        self.lblStatus.isHidden=true
        
        tblCorporateOrderHistory.tableFooterView=UIView()
        tblCorporateOrderHistory.estimatedRowHeight = 350
        tblCorporateOrderHistory.rowHeight = UITableViewAutomaticDimension
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        self.hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchBar.text?.isEmpty)!{
            searchActive = false
            tblCorporateOrderHistory.reloadData()
        } else {
            
            searchActive = true
            filtered.removeAll()
            
            for i in 0...arrOrderList.count-1{
                
                let dict = arrOrderList[i]
                let vendorDict = dict["vendor"] as! NSDictionary
                let tmp = vendorDict["name"] as! NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                
                if(range.location != NSNotFound){
                    filtered.append(dict)
                }else{
                }
            }
        }
        tblCorporateOrderHistory.reloadData()
    }
    
    
    func callOrderHistoryListApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.corporate_user_history_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
               
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        self.arrOrderList = dict["response"] as! [NSDictionary]
                        
                        
                        //self.arrOrderList = arrResponse.reversed()
                        
                        if(self.arrOrderList.count==0){
                            self.lblStatus.isHidden=false
                            self.tblCorporateOrderHistory.isHidden=true
                            
                        }else{
                            self.lblStatus.isHidden=true
                            self.tblCorporateOrderHistory.isHidden=false
                            self.tblCorporateOrderHistory.reloadData()
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideMenuButtonPressed(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension customerOrderHistryVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(searchActive) {
            return filtered.count
        }
        
        return arrOrderList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCorporateOrderHistory.dequeueReusableCell(withIdentifier: "corporateOrderTblCell") as! corporateOrderTblCell
        
        var dict : NSDictionary!
        
        
        if(searchActive){
            if(filtered.count>0){
               dict = filtered[indexPath.section]
            }
        }else{
            dict = arrOrderList[indexPath.section]
        }
        
        if(dict != nil){
            
            cell.lblTotalAmount.isHidden=true
            cell.lblAmount.isHidden=true
            
            
            cell.lblBookingId.text = "  Booking Id : \((dict["booking_id"] as? String)!)"
            cell.lblOrderId.text = "  Order Id : \((dict["order_id"] as? String)!)"
            let vendorDict = dict["vendor"] as! NSDictionary
            cell.lblName.text = vendorDict["name"] as? String
            cell.lblNumber.text = vendorDict["phone"] as? String
            
            cell.lblAddress.text=vendorDict["address"] as? String
            cell.lblDeliveryCode.text = "  Delivery Code : \((dict["pin"] as? String)!)"
            
            
            
            cell.lblTeaCount.text = "  No of Tea : \((dict["no_of_tea"] as? String)!)"
            cell.lblCoffeeCount.text = "  No of Coffee : \((dict["no_of_coffee"] as? String)!)"
            
            
            
            
            // cell.lblDate.text="Date : \((dict["date"] as? String)!)"
            let dateAsString = "\((dict["date"] as? String)!)"
            let strDae : String = ""
            cell.lblDate.text = "  Date: \(strDae.changeDateFormat(strDate:dateAsString))"
            
            let timeAsString = "\((dict["time"] as? String)!)"
            let strr : String = ""
            cell.lblTime.text = "  Time :\(strr.changeTimeFormat(strDate: timeAsString))"
            
            let status = dict["status"] as! String
            if(status=="1"){
                cell.lblOrderStatus.text = "Initialized"
                cell.lblDeliveryCode.isHidden=false
                cell.viewHeightConstraint.constant = 180
                
            }else if(status=="2"){
                cell.lblOrderStatus.text = "Pending"
                cell.lblDeliveryCode.isHidden=false
                cell.viewHeightConstraint.constant = 180
                
            }else if(status=="3"){
                cell.lblOrderStatus.text = "Delivered"
                cell.lblDeliveryCode.isHidden=true
                cell.viewHeightConstraint.constant = 155
               
            }
            else if(status=="4"){
                cell.lblOrderStatus.text = "Cancelled"
                cell.lblOrderStatus.textColor = UIColor.red
                cell.lblDeliveryCode.isHidden=false
                cell.viewHeightConstraint.constant = 180
            }

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
    }
    
    
}

class corporateOrderTblCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderId : UILabel!
    @IBOutlet weak var lblBookingId : UILabel!
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblCoffeeCount : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblTotalAmount : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblNumber : UILabel!
    @IBOutlet weak var lblOrderStatus : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblDeliveryCode : UILabel!
    
    @IBOutlet weak var viewHeightConstraint : NSLayoutConstraint!
    
    
    var strUrl : String = ""
    
}




