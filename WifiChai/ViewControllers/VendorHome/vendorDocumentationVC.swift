
import UIKit
import Photos

class vendorDocumentationVC: UIViewController ,  UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    @IBOutlet weak var lblLineStore : UILabel!
    @IBOutlet weak var lblLineBankDetail : UILabel!
    
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var viewDocuments : UIView!
    @IBOutlet weak var viewBankDetails : UIView!
    
    @IBOutlet weak var btnBankDetails : UIButton!
    @IBOutlet weak var btnDocuments : UIButton!
    @IBOutlet weak var tblDocuments : UITableView!
    
    
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtAcctNo : UITextField!
    @IBOutlet weak var txtIfscCode : UITextField!
    @IBOutlet weak var txtBankName : UITextField!
    @IBOutlet weak var txtBranchAdd : UITextField!
    
    @IBOutlet weak var btnContinue : UIButton!
    var arrDocuments_List : [NSDictionary] = []
    
    
    var isDocVerified : Bool = false
    
    var imgPicker = UIImagePickerController()
    
    var chosenImage = UIImage()
    var asset : PHAsset!
    var imgName : String = ""
    var isFilenameEmpty : String = ""
    
    var document_id : String = ""

    
    
    var Pageint : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        
        
        imgPicker.delegate = self
        
        viewBankDetails.isHidden=true
        lblLineBankDetail.backgroundColor=UIColor.clear
        btnBankDetails.setTitleColor(.black, for: .normal)
        
        tblDocuments.tableFooterView=UIView()
        btnContinue.isEnabled=false
        btnContinue.isUserInteractionEnabled=false
        
        DispatchQueue.main.async {
            self.callGetBankDetailApi()
            self.callGetDocumentListApi()
        }
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clicknBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func clickOnBankingBtn(_ sender : UIButton){
        if (Pageint < 1) {
            viewMain.slideInFromRight()
            lblLineStore.slideInFromLeft()
            Pageint += 1
            self.changeView()
        }
    }
    
    @IBAction func clickOnDocumentsBtn(_ sender : UIButton){
        if (Pageint > 0) {
            viewMain.slideInFromLeft()
            lblLineBankDetail.slideInFromRight()
            Pageint -= 1
            self.changeView()
            
        }
    }
    
    @IBAction func clickOnLogoutBtn(_ sender : UIButton){
       let story = AppStoryboard.Main.instance
       let login = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(login, animated: true, completion: nil)
    }

    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        
        if (sender.direction == .left) {
            
            if (Pageint < 1) {
                viewMain.slideInFromRight()
                lblLineStore.slideInFromLeft()
                Pageint += 1
                self.changeView()
            }
            
        }
        
        
        if (sender.direction == .right) {
            
            if (Pageint > 0) {
                viewMain.slideInFromLeft()
                lblLineBankDetail.slideInFromRight()
                Pageint -= 1
                self.changeView()
                
            }
            
        }
    }
    
    
    func changeView()
    {
        if (viewBankDetails.isHidden==true) {
            viewBankDetails.isHidden=false
            viewDocuments.isHidden=true
            
            lblLineBankDetail.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            btnBankDetails.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: .normal)
            
            lblLineStore.backgroundColor=UIColor.clear
            btnDocuments.setTitleColor(.black, for: .normal)
            
            
            
            
        }
        else if (viewDocuments.isHidden==true){
            viewBankDetails.isHidden=true
            viewDocuments.isHidden=false
            
            lblLineStore.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            btnDocuments.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: .normal)
            
            lblLineBankDetail.backgroundColor=UIColor.clear
            btnBankDetails.setTitleColor(.black, for: .normal)
        }
    }
    


}

extension vendorDocumentationVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
         return arrDocuments_List.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblDocuments.dequeueReusableCell(withIdentifier: "vendorDocTableCell") as! vendorDocTableCell
        
        let dict = arrDocuments_List[indexPath.section]
        cell.lblDocumentName.text = dict["document_name"] as? String
        document_id = dict["document_id"] as! String
        
        let document_status = dict["document_status"] as! String
        if(document_status=="1"){
            cell.lblDocStatus.text="Add File"
        }else if(document_status=="2"){
            cell.lblDocStatus.text="Pending"
        }else if(document_status=="3"){
            cell.lblDocStatus.text="Verified"
        }else if(document_status=="4"){
            cell.lblDocStatus.text="Rejected"
        }
        
        
        let doc_url = dict["document_url"] as? String
        if(doc_url==""){
            cell.btnUpload.setImage(UIImage(named: "upload"), for: .normal)
            cell.imgDocuments.image=UIImage(named: "gallery_placeholder")
        }
        else{
            
            cell.btnUpload.setImage(UIImage(named: "editFile"), for: .normal)
            
            cell.imgDocuments.setIndicatorStyle(.gray)
            cell.imgDocuments.setShowActivityIndicator(true)
            
            if let imageURL = URL(string:"\(doc_url!)") {
                cell.imgDocuments.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
            }
        }
        
        
        cell.btnUpload.addTarget(self, action: #selector(self.callImgUploadApi(sender:)), for: .touchUpInside)
        cell.btnUpload.tag=indexPath.section
        
       
        
        return cell
    }
    
    @objc func callImgUploadApi(sender: UIButton){
        let buttonTag = sender.tag
        ApiResponse.CameraGallery(controller: self, imagePicker: imgPicker)
    }
    
    func saveImg(chooseImg : UIImage){
        
        showHud()
        
        let url = NSURL(string: Constant.uploadDocument_URL)
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var body = Data()
        
        var imgfieName : String = ""
        if (takePhoto == "Gallery") {
            if(asset==nil){
                imgfieName="FirstImg.png"
            }
            else{
                let fname = (asset.value(forKey: "filename"))!   //"test.png"
                
                let words = (fname as! String).components(separatedBy: ".")
                let fnm=words[0]
                
                imgfieName="\(fnm).png"
            }
            
        }
        else {
            imgfieName="\(imgName).png"
        }
        
        print(imgfieName)
        let mimetype = "image/png"
        
        
        
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let parameters : [String : String] = ["user_id" : "\(userid)","document_id":"\(document_id)"]
        
        for (key, value) in parameters
        {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        
        let image_data = UIImageJPEGRepresentation(chooseImg, 0.5)
        //let image_data = UIImageJPEGRepresentation(chosenImage, 0.5)
        if(image_data == nil)
        {
            return
        }
        //define the data post parameter
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"document_image\"; filename=\"\(imgfieName)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            
            
            
            
            DispatchQueue.main.async {
                self.hideHUD()
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                let dd = try! JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                print("Profile img Response : \(dd)")
                let status = dd["status"] as! Bool
                let message = dd["message"] as! String
                if (status == false) {
                    ApiResponse.alert(title: "", message: message, controller: self)
                }
                else {
                    ApiResponse.alert(title: "", message: message, controller: self)
                    
                    self.document_id=""
                    
                }
            }
        }
        task.resume()
    }
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

class vendorDocTableCell : UITableViewCell{
    
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var lblDocStatus: UILabel!
    @IBOutlet weak var btnUpload: UIButton!
    @IBOutlet weak var imgDocuments : UIImageView!
}

extension vendorDocumentationVC {
    
    //Get bank details of user
    func callGetBankDetailApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.getBankDetails_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        let responseDict = dict["response"] as! NSDictionary
                        
                        let bankDetails = responseDict["bank_details"] as! NSDictionary
                        self.txtName.text = bankDetails["account_holder_name"] as? String
                        
                        self.txtAcctNo.text = bankDetails["account_number"] as? String
                        self.txtBankName.text = bankDetails["bank_name"] as? String
                        self.txtBranchAdd.text = bankDetails["branch_address"] as? String
                        self.txtIfscCode.text = bankDetails["ifsc_code"] as? String
                        self.txtName.text = bankDetails["account_holder_name"] as? String
                        
                        self.isDocVerified = responseDict["is_document_verified"] as! Bool
                        if(self.isDocVerified==false){
                            self.btnContinue.isEnabled=false
                            self.btnContinue.isUserInteractionEnabled=false
                            self.btnContinue.backgroundColor=UIColor.lightGray
                        }else{
                            self.btnContinue.isEnabled=true
                            self.btnContinue.isUserInteractionEnabled=true
                            self.btnContinue.backgroundColor=UIColor.red
                        }
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    
    //Send bank details
    @IBAction func clickOnSubmitBtn(_ sender : UIButton){
        
        if((txtName.text == "") || (txtName.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Account holder name.", controller: self)
            
        }
        else if((txtAcctNo.text == "") || (txtAcctNo.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Account number.", controller: self)
        }
        else if((txtIfscCode.text == "") || (txtIfscCode.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter IFSC code.", controller: self)
        }
        else if((txtBankName.text == "") || (txtBankName.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Bank Name.", controller: self)
        }
        else if((txtBranchAdd.text == "") || (txtBranchAdd.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Branch Address.", controller: self)
        }
            
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            self.callSetBankDetailsApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
        
    }
    
    func callSetBankDetailsApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)","account_number":"\(txtAcctNo.text!)","account_holder_name":"\(txtName.text!)","ifsc_code":"\(txtIfscCode.text!)","bank_name":"\(txtBankName.text!)","branch_address":"\(txtBranchAdd.text!)"] as NSDictionary
        
        
        ApiResponse.onResponsePost(url: Constant.setBankDetails_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    
    //Get document list of user
    func callGetDocumentListApi(){
        
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.document_list_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let responseDict = dict["response"] as! NSDictionary
                        
                        self.arrDocuments_List = responseDict.object(forKey: "document_list") as! [NSDictionary]
                        self.tblDocuments.reloadData()
                        
                        
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
}
