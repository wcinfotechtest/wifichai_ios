//
//  ChangePassViewController.swift
//  WifiChai
//
//  Created by MacbookPro on 09/06/18.
//  Copyright © 2018 mavencluster. All rights reserved.
//

import UIKit

class ChangePassViewController: UIViewController {
    
    var strMobileNo : String = ""
    @IBOutlet weak var lblMobileNo : UILabel!
    
    @IBOutlet weak var txtOtp1 : UITextField!
    @IBOutlet weak var txtOtp2 : UITextField!
    @IBOutlet weak var txtOtp3 : UITextField!
    @IBOutlet weak var txtOtp4 : UITextField!

    
    @IBOutlet weak var txtNewPass : UITextField!
    @IBOutlet weak var txtcnfrmPass : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        lblMobileNo.text = "PLEASE TYPE VERIFICATION CODE SENT TO  \(strMobileNo)"
        
        
        txtOtp1.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtOtp2.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtOtp3.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtOtp4.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        // Do any additional setup after loading the view.
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField){
        
        let text = textField.text
        
        if text?.count == 1{
            switch textField{
            case txtOtp1:
                txtOtp2.becomeFirstResponder()
            case txtOtp2:
                txtOtp3.becomeFirstResponder()
            case txtOtp3:
                txtOtp4.becomeFirstResponder()
            case txtOtp4:
                txtOtp4.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    
    
    @IBAction func clickOnResendbtn(_ sender : UIButton){
        //call resend otp api
    }
    
    @IBAction func clickOnSubmitBtn(_ sender : UIButton){
         if((txtNewPass.text == "") || (txtNewPass.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Password.", controller: self)
        }
        else if((txtcnfrmPass.text == "") || (txtcnfrmPass.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Password again.", controller: self)
        }
       
        else if(!(ApiResponse.isPwdLenth(password: txtNewPass.text!))){
            ApiResponse.alert(title: "Alert", message: "Password should be minimum of 7 digit.", controller: self)
        }
         else if(txtNewPass.text != txtcnfrmPass.text){
            ApiResponse.alert(title: "Alert", message: "PAssword and re-password are mismatch", controller: self)
         }
            
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            self.callChangePassApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
    }
    
    func callChangePassApi(){
        let txtOtp = "\(txtOtp1.text!)\(txtOtp2.text!)\(txtOtp3.text!)\(txtOtp4.text!)"
        let param = ["mobile" : "\(strMobileNo)","new_password" : "\(txtNewPass.text!)","otp":"\(txtOtp)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.chnagePass_URL, parms: param) { (dict, error) in
            if(error == ""){
                print(dict)
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                      
                        let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                            
                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        self.present(login, animated: true, completion: nil)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
extension ChangePassViewController : UITextFieldDelegate{
    
     func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
    textField.backgroundColor = UIColor.clear
    textField.layer.borderColor = UIColor.white.cgColor
    textField.layer.borderWidth = 3.0
    
    textField.textColor=UIColor.white
    
    return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    
    textField.backgroundColor = UIColor.lightGray
    textField.layer.borderColor = UIColor.lightGray.cgColor
    textField.layer.borderWidth = 3.0
    
    textField.textColor=UIColor.black
    
    return true
    }
    
    
}
