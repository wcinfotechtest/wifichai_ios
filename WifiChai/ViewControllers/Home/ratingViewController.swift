

import UIKit

class ratingViewController: UIViewController {
    
    
    @IBOutlet var btnRating : [UIButton]!
    
    var rating : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        for star in self.btnRating as [UIButton]
        {
            star.addTarget(self, action: #selector(ratingViewController.buttonClicked(button:)), for: .touchUpInside)
        }
        
       
        // Do any additional setup after loading the view.
    }
    
    @objc func buttonClicked(button: UIButton)
    {
        for star in self.btnRating as [UIButton]
        {
            star.setBackgroundImage(UIImage (named: "emptystar"), for: UIControlState.normal)
        }
        
        for i in 0  ... (btnRating.index(of: button)!)
        {
            btnRating[i].setBackgroundImage(UIImage (named: "fullstar"), for: UIControlState.normal)
            rating = btnRating.index(of: button)!
            rating = rating +  1
            
        }
    }
    
    @IBAction func clickOnSubmitBtn(_ sender : UIButton){
        
        showHud()
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)","order_id":"","rating":"\(rating)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.user_rating_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                       // let responseDict = dict["response"] as! NSDictionary
                        //print(responseDict)
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
