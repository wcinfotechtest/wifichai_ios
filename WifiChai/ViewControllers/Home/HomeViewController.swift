
import UIKit
import GoogleMaps
import CoreLocation

class HomeViewController: BaseViewController , CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView : GMSMapView!
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
  //  lazy var mapView = GMSMapView()
    
    @IBOutlet weak var btnTea : UIButton!
    @IBOutlet weak var btnCoffee : UIButton!
    @IBOutlet weak var btnMarker : UIButton!
    
    @IBOutlet weak var markerView : UIView!
    
    @IBOutlet weak var selectOrderView : UIView!
    
    
    @IBOutlet var ratingView: [UIImageView]!
    @IBOutlet weak var lblVendorName : UILabel!
    @IBOutlet weak var lblVendorEstimatedTime : UILabel!
    @IBOutlet weak var imgVendor : UIImageView!
    
    var vendorId : String = ""
    var vendorAddress : String = ""
    var vendorLatitude : String = ""
    var vendorLongitude : String = ""
    
    
    
    @IBOutlet weak var collViewOrder : UICollectionView!
    var arrColl = [String]()
    
    var indexArray = [Int]()
    var flowLayout : LGHorizontalLinearFlowLayout!
    var arrVendorDetails : [NSDictionary] = []
    var locationManager:CLLocationManager!
    
    var lat: Double = 0.0
    var long: Double = 0.0
    
    var selectProduct : String = ""
    
    
    
    @IBOutlet weak var lblVendorNameonSelect : UILabel!
    @IBOutlet weak var txtQuantity : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        
        let dict = userDef.value(forKey: "userInfo") as! NSDictionary
        let usertype=dict["user_type"] as! String
        userDef.set(usertype, forKey: "userType")
        
        
        btnTea.setRoundedCorners([.topLeft, .bottomLeft], radius: 16.0)
        btnCoffee.setRoundedCorners([.topRight, .bottomRight], radius: 16.0)
        
        mapView.delegate=self
        
        selectProduct = "1"
        self.determineMyCurrentLocation()
        
        markerView.isHidden=true
        selectOrderView.isHidden=true
        
        self.flowLayout = LGHorizontalLinearFlowLayout.configureLayout(collectionView: self.collViewOrder, itemSize: CGSize(width:40, height:40), minimumLineSpacing: 2)
        
        
        for i in 1...99{
            arrColl.append("\(i)")
        }
        
        
        ApiResponse.decorateTextField(txtQuantity, keyBoardType: .default, placeHolder: "Quantity", isSecureText: false)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func determineMyCurrentLocation() {
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton=true

        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
      //  mapView.camera = GMSCameraPosition(target: userLocation.coordinate, zoom: 13, bearing: 0, viewingAngle: 0)
        
        
        let lat = "22.7196"
        let long = "75.8577"
        
    //    mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude), zoom: 13, bearing: 0, viewingAngle: 0)
        
        mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!), zoom: 13, bearing: 0, viewingAngle: 0)
        
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
       // self.callVendorDetailApi(strLat: "\(userLocation.coordinate.latitude)", strLong: "\(userLocation.coordinate.longitude)")
        
        self.callVendorDetailApi(strLat: lat, strLong: long)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Error \(error.localizedDescription)")
    }
    
    
    @IBAction func clickOnbtnTeaCoffee(_ sender : UIButton){
            if (sender.tag == 0){
                selectProduct = "1"
                btnTea.setTitle("Tea", for: UIControlState.normal)
                btnTea.setTitleColor(UIColor.white, for: UIControlState.normal)
                btnTea.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
                
                btnCoffee.backgroundColor=UIColor.white
                btnCoffee.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: UIControlState.normal)
            }
            else if (sender.tag == 1){
                selectProduct = "2"
                btnCoffee.setTitle("Coffee", for: UIControlState.normal)
                btnCoffee.setTitleColor(UIColor.white, for: UIControlState.normal)
                btnCoffee.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
                btnTea.backgroundColor=UIColor.white
                btnTea.setTitleColor(UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0), for: UIControlState.normal)
            }
    }
    
    
    func callVendorDetailApi(strLat : String , strLong : String){
        
        let param = ["latitude" : "\(strLat)", "longitude":"\(strLong)"] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.get_vendor_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        self.arrVendorDetails = dict["response"] as! [NSDictionary]
                        print(self.arrVendorDetails)
                        
                        for i in 0...self.arrVendorDetails.count-1{
                            let vendor_marker = GMSMarker()
                            
                            let dict = self.arrVendorDetails[i] 
                            let latt = dict["latitude"] as! String
                            let long = dict["longitude"] as! String
                            
                            vendor_marker.position = CLLocationCoordinate2D(latitude: Double(latt)!, longitude: Double(long)!)
                            
                            vendor_marker.title = dict["user_id"] as? String
                            vendor_marker.icon = self.image(UIImage(named: "vendor_marker")!, scaledToSize: CGSize(width: 30, height: 42))
                            
                            vendor_marker.accessibilityValue="\(i)"
                            
                            vendor_marker.map = self.mapView
                            
                        }

                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }

    //Image function
     func image(_ originalImage:UIImage, scaledToSize:CGSize) -> UIImage {
        if originalImage.size.equalTo(scaledToSize) {
            return originalImage
        }
        UIGraphicsBeginImageContextWithOptions(scaledToSize, false, 0.0)
        originalImage.draw(in: CGRect(x: 0, y: 0, width: scaledToSize.width, height: scaledToSize.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideMenuButtonPressed(sender)
    }
    
    
   

}



extension HomeViewController : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView?{
        return UIView()
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
      
        
        locationMarker = marker
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
        
        markerView.center = self.mapView.projection.point(for: location)
        markerView.center.y = markerView.center.y - sizeForOffset(view: markerView)
        markerView.isHidden=false
        
        let userid = marker.title
        for i in 0...self.arrVendorDetails.count-1{
            let dict = self.arrVendorDetails[i]
            if(userid == dict["user_id"] as? String){
                lblVendorName.text=dict["name"] as? String
                lblVendorEstimatedTime.text=dict["estimated_time"] as? String
                let img=dict["profile_pic"] as? String
                imgVendor.setIndicatorStyle(.gray)
                imgVendor.setShowActivityIndicator(true)
                if let imageURL = URL(string:"\(img!)") {
                    imgVendor.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"))
                    
                }
                let rating=dict["rating"] as! NSNumber
                let myInt = (rating).doubleValue
                ApiResponse.callRating(myInt, starRating: ratingView)
                
                btnMarker.tag=i
                
               
            }
        }
        return true
    }
    
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            
            markerView.center = self.mapView.projection.point(for: location)
            markerView.center.y = markerView.center.y - sizeForOffset(view: markerView)
        }
    }
    
    // MARK: Needed to create the custom info window (this is optional)
    func sizeForOffset(view: UIView) -> CGFloat {
        return  105.0
    }
    
    
    @IBAction func clickOnBtnTap(_ sender : UIButton){
        
        let dict = self.arrVendorDetails[sender.tag]
        self.lblVendorNameonSelect.text=dict["name"] as? String
        vendorId=dict["user_id"] as! String
        vendorAddress=dict["address"] as! String
        vendorLatitude=dict["latitude"] as! String
        vendorLongitude=dict["longitude"] as! String
        
       
        
        
        
        self.markerView.isHidden=true
        self.selectOrderView?.frame = CGRect(x: 12, y: 428, width: 351, height: 251)
        self.selectOrderView.fadeIn()
        self.selectOrderView.isHidden=false
        //collViewOrder.scrollToItem(at:IndexPath(item: 0, section: 0), at: .left, animated: true)
        let indexPath = IndexPath(item: 2, section: 0)
        self.collViewOrder.scrollToItem(at: indexPath, at: [.left], animated: true)
        
        
        
        
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("infowindow tapped")
    }
}


extension HomeViewController : UICollectionViewDelegate , UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrColl.count
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collViewOrder.dequeueReusableCell(withReuseIdentifier: "placeOrderCollCell", for: indexPath as IndexPath) as! placeOrderCollCell
        
        print(self.arrColl[indexPath.item])
        
        collectionViewCell.lblText.text = self.arrColl[indexPath.item]
        
        collectionViewCell.lblText.tag=indexPath.row
        if indexArray.contains(indexPath.row){
            collectionViewCell.lblText.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            collectionViewCell.lblText.textColor=UIColor.white
            
        }else{
            collectionViewCell.lblText.textColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            collectionViewCell.lblText.backgroundColor=UIColor.white
        }
        
        return collectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
            return
        }
        
        indexArray.append(indexPath.row)
        let selectedCell = collectionView.cellForItem(at: indexPath)  as? placeOrderCollCell
        
        selectedCell?.lblText.backgroundColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        selectedCell?.lblText.textColor=UIColor.white
        
        txtQuantity.text = self.arrColl[indexPath.item]
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        
        if(indexArray.count>0){
            indexArray.removeAll()
        }
        collectionView.deselectItem(at: indexPath, animated: true)
        
        
        let selectedCell = collectionView.cellForItem(at: indexPath)  as? placeOrderCollCell
        selectedCell?.lblText.textColor=UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        selectedCell?.lblText.backgroundColor=UIColor.white
       
        
    }
    
}

class placeOrderCollCell : UICollectionViewCell{
    @IBOutlet weak var lblText : UILabel!
   
}

extension HomeViewController {
    
    //Call Place Order Api
    func callPlaceOrderApi(){
        
       // print(Constant.place_order_URL)
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)","vendor_id":"\(vendorId)","product_id":"\(selectProduct)","quantity":"\(txtQuantity.text!)","address":"\(vendorAddress)","latitude":"\(vendorLatitude)","longitude":"\(vendorLongitude)"] as NSDictionary
        
       // print(param)
        
        ApiResponse.onResponsePost(url: Constant.place_order_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool

                if (status==true){
                    
                    OperationQueue.main.addOperation {

                        self.hideHUD()

                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        
                        self.selectOrderView.isHidden=true

                        let cnfrm = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmOrderVC") as! ConfirmOrderVC
                        
                        cnfrm.dictPlaceOrderDetails=dict["response"] as! NSDictionary
                         self.present(cnfrm, animated: true, completion: nil)

                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
        
    }
    
    @IBAction func clickOnPlaceOrderBtn(_ sender : UIButton){
        
        if(selectProduct == ""){
            ApiResponse.alert(title: "", message: "Please choose either Tea or Coffee", controller: self)
        }
        else if((txtQuantity.text == "") || (txtQuantity.text?.count)==0){
            ApiResponse.alert(title: "", message: "Please provide quantity", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            showHud()
            self.callPlaceOrderApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
        
      
    }
}
