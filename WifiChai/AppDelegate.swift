

import UIKit
import ReachabilitySwift
import SystemConfiguration
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import MBProgressHUD
import GoogleMaps
import GooglePlaces
import UserNotifications
import Firebase





var reach : Reachability = Reachability()!
var userDef = UserDefaults.standard
var currentLang : String = ""
let APPLE_LANGUAGE_KEY = "AppleLanguages"

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

enum AppStoryboard : String {
    case Main = "Main"
    case Vendor = "vendor"
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}

let requestIdentifier = "actionTap"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate{

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "738161132151-1uicij6934n9bo5lmudqpchhjupao7eh.apps.googleusercontent.com"
        
        GMSServices.provideAPIKey("AIzaSyDGZyXJmLCGTwlmI_g6M_PxBBgQS-hoHCI")
        GMSPlacesClient.provideAPIKey("AIzaSyDGZyXJmLCGTwlmI_g6M_PxBBgQS-hoHCI")
        
//
//        if #available(iOS 10.0, *) {
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().delegate = self
//            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { _, _ in })
//        } else {
//            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
        
        application.registerForRemoteNotifications()
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    OperationQueue.main.addOperation {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else{
                    //Do stuff if unsuccessful...
                }
//                let action1 = UNNotificationAction(identifier: "actionTap", title: "Chat", options: [.foreground])
//                let category = UNNotificationCategory(identifier: "CategoryIdentifier", actions: [action1], intentIdentifiers: [], options: [])
//                UNUserNotificationCenter.current().setNotificationCategories([category])
            })
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil);
            UIApplication.shared.registerUserNotificationSettings(setting);
            UIApplication.shared.registerForRemoteNotifications();
        }
        
        Messaging.messaging().delegate = self
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.MessagingRegistrationTokenRefreshed, object: nil)
        
        
        
        let langArray = userDef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let lang = langArray.firstObject as! String
        if(lang == "en")
        {
            currentLang = "0"
        }
        else
        {
            currentLang = "1"
        }
        
        
      //  L012Localizer.DoTheSwizzling()
        
        
        let session = userDef.object(forKey: "session") as? String
        if (session == "") || (session == nil)
        {
            //login vc
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC  = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = loginVC
            self.window?.makeKeyAndVisible()
        }
        else if (session == "Vendor_login"){
            
            let mainStoryboard = AppStoryboard.Vendor.instance
            let homeVC  = mainStoryboard.instantiateViewController(withIdentifier: "VendorHomeVC") as! VendorHomeVC
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = homeVC
            self.window?.makeKeyAndVisible()
            
        } else if(session == "Corporate_login"){
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC  = mainStoryboard.instantiateViewController(withIdentifier: "CorporateCustHomeVC") as! CorporateCustHomeVC
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = homeVC
            self.window?.makeKeyAndVisible()
        }
        else{
            //home vc
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC  = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = homeVC
            self.window?.makeKeyAndVisible()
            
        }
        
      //  self.callLocalNotification(msgContent: "hello wifichai")
        
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        return true
    }
    
    func add111()
    {
        let mainStoryboard = AppStoryboard.Vendor.instance
        let homeVC  = mainStoryboard.instantiateViewController(withIdentifier: "VendorHomeVC") as! VendorHomeVC
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = homeVC
        self.window?.makeKeyAndVisible()
    }
    
    
    
    //Handle push notification:
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("Registration succeeded! Token: ", token)
        let a = Messaging.messaging().fcmToken
        print("FCM token: \(a ?? "")")
        
        
        
      //  Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
    }
    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration failed!")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        //for background
    }
    
   
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]){
        
        //for foreground
        print(userInfo)
    }
    
    // Firebase notification received
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        
        // custom code to handle push while app is in the foreground
        print("Handle push from foreground, received: \n \(notification.request.content)")
        
        print("Notification being triggered")
        //You can either present alert ,sound or increase badge while the app is in foreground too with ios 10
        //to distinguish between notifications
        if notification.request.identifier == requestIdentifier{
            
            completionHandler( [.alert,.sound,.badge])
            
        }
        
       
        
    }
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle tapped push from background, received: \n \(response.notification.request.content)")
        completionHandler()
    }
    
    func showAlertAppDelegate(title: String, message: String, buttonTitle: String, window: UIWindow) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        window.rootViewController?.present(alert, animated: false, completion: nil)
    }
    
   
    
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL?, sourceApplication: sourceApplication, annotation: annotation)
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return googleDidHandle || facebookDidHandle

    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        self.connectToFcm()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification)
    {
        if let refreshedToken = Messaging.messaging().fcmToken{
              print("InstanceID token: \(refreshedToken)")
        }
        connectToFcm()
    }
    
    func connectToFcm(){
        Messaging.messaging()
    }

    //// IPV6 config code
    func ipv6Reachability() -> SCNetworkReachability?{
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

}

extension UIViewController {
    func showHud(_ message: String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
        hud.isUserInteractionEnabled = false
    }
    
    func showHud(){
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.bezelView.style = .solidColor
        hud.activityIndicatorColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
    }
    
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        //Messaging.messaging().subscribe(toTopic: "/topics/nutriewell_live")
       // Messaging.messaging().shouldEstablishDirectChannel = true
        connectToFcm()
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        
        let state: UIApplicationState = UIApplication.shared.applicationState
        let dict = remoteMessage.appData as NSDictionary
        let tag = dict["tag"] as! Int
        let title = dict["title"] as? String
        
        let usertype = userDef.value(forKey: "userType") as! String
        
        
        //personal customer
        if(tag == 101){    //when customer place order
            if state == .background {   // background
                let mainStoryboard = AppStoryboard.Vendor.instance
                let homeVC  = mainStoryboard.instantiateViewController(withIdentifier: "VendorHomeVC") as! VendorHomeVC
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = homeVC
                self.window?.makeKeyAndVisible()
                
            }
            else if state == .active {  // foreground
                self.callLocalNotification(msgContent: title!)
            }
        }
        else if(tag == 102){    //when customer order successful open rating screen
            
            
            if state == .background {   // background
                if (usertype=="2"){ // personal
                    
                }
                else if(usertype=="3"){ //corporate
                    
                }
                
            }
            else if state == .active {  // foreground
                
                if (usertype=="2"){ // personal
                    
                }
                else if(usertype=="3"){ //corporate
                    
                }
            }
            
            
            
            
        }
        else if(tag == 103){    //when customer order cancel
            if (usertype=="2"){ // personal
                
            }
            else if(usertype=="3"){ //corporate
                
            }
        }
       //Corporate customer
        else if(tag == 105){ //Document verification notification
            if (usertype=="2"){ // personal
                
            }
            else if(usertype=="1"){ //vendor
                
            }
        }
        else if(tag == 106){ //Bank detail verification notification
            if (usertype=="2"){ // personal
                
            }
            else if(usertype=="1"){ //vendor
                
            }
        }
        else if(tag == 107){ //Company detail verification notification
           
        }
        else if(tag == 108){ //Order accept notification
            
        }
        else if(tag == 109){ //Order deny notification
            
        }
            
        //vendor customer
        else if(tag == 104){ //Corporate booking assign notification
            
        }
        
    }
    
    
    func callLocalNotification(msgContent:String){
            if #available(iOS 10.0, *){
                //UNUserNotificationCenter.current().delegate = self
                let center = UNUserNotificationCenter.current()
                let content = UNMutableNotificationContent()
                content.title = "Wifi Chai"
                content.body = msgContent
                content.sound = UNNotificationSound.default()
                
                
                
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
                let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
                
                center.add(request)
                
            }
            else {
                // ios 9
                let notification = UILocalNotification()
               // notification.fireDate = NSDate(timeIntervalSinceNow: 5) as Date
                notification.alertBody = msgContent
                //notification.alertAction = "be awesome!"
                notification.soundName = UILocalNotificationDefaultSoundName
                UIApplication.shared.scheduleLocalNotification(notification)
            }
    }
}
