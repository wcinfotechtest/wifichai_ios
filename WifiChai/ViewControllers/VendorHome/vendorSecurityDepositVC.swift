
import UIKit

class vendorSecurityDepositVC: UIViewController {
    
    @IBOutlet weak var tblSecurity:UITableView!
    var arrPaymentHistoryList : NSMutableArray = []
    @IBOutlet weak var lblStatus : UILabel!
    
     @IBOutlet weak var lblRemainingAmount : UILabel!
    
    @IBOutlet weak var txtAddAmount : UITextField!
    @IBOutlet weak var viewAddAmnt : UIView!
    
    var merchant:PGMerchantConfiguration!

    override func viewDidLoad() {
        super.viewDidLoad()
        tblSecurity.tableFooterView=UIView()
        
        tblSecurity.isHidden=true
        self.lblStatus.isHidden=true
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        viewAddAmnt.isHidden=true
        
        
        ApiResponse.decorateTextField(txtAddAmount, keyBoardType: .numberPad, placeHolder: "Amount", isSecureText: false)
        DispatchQueue.main.async {
            self.getPaymentHistoryApi()
        }
        
        
        tblSecurity.estimatedRowHeight = 109.0
        tblSecurity.rowHeight = UITableViewAutomaticDimension
        
        
       

    }
    
    func callVendorDepositApi(strOrderId : String , strTransID : String , strTxnAmount : String , strStatus : String){
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["vendor_id" : "\(userid)","amount":"\(strTxnAmount)","transaction_id":"\(strTransID)","status":"\(strStatus)","order_id":"\(strOrderId)"] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.vendor_deposite_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        let response = dict["response"] as! NSDictionary
                        print(response)
                        
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func clikcOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnAddAmtBtn(_ sender : UIButton){
        viewAddAmnt.isHidden=false
    }
    
    @IBAction func clickOnDismissViewBtn(_ sender : UIButton){
        viewAddAmnt.isHidden=true
    }
    
    @IBAction func clickOnAddBtn(_ sender : UIButton){
        
        
        if(txtAddAmount.text == "" || txtAddAmount.text?.count==0){
            ApiResponse.alert(title: "", message: "Please enter amount.", controller: self)
        }else{
            if(self.lblRemainingAmount.text! < "500"){
                if(txtAddAmount.text! < "2000"){
                    ApiResponse.alert(title: "", message: "Add minimum 2000 rupees.", controller: self)
                }
                else{
                    if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN{
                        showHud()
                        self.setMerchant()
                    }
                    else{
                        ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
                    }
                }
                
            }else{
                if(txtAddAmount.text! < "500"){
                    ApiResponse.alert(title: "", message: "Add minimum 500 rupees.", controller: self)
                }
                else{
                    if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN{
                        showHud()
                        self.setMerchant()
                    }
                    else{
                        ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
                    }
                }
            }
                
        }
    }
    
    
    func getPaymentHistoryApi(){
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["vendor_id" : "\(userid)"] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.vendorpayment_history_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        let response = dict["response"] as! NSDictionary
                        
                        print(response)
                        
                        self.lblRemainingAmount.text="INR : \((response["total_amount"] as? NSNumber)!)"
                            
                        if let res = response["transaction_history"] as? NSArray {
                            for i in 0..<(res.count){
                                if let dict = res[i] as? NSDictionary {
                                    self.arrPaymentHistoryList.add(dict)
                                }
                            }
                            if(self.arrPaymentHistoryList.count==0){
                                self.lblStatus.isHidden=false
                                self.tblSecurity.isHidden=true
                                
                            }else{
                                self.lblStatus.isHidden=true
                                self.tblSecurity.isHidden=false
                                self.tblSecurity.reloadData()
                            }
                        }
                        
                        
                        
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
   


}

extension vendorSecurityDepositVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arrPaymentHistoryList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tblSecurity.dequeueReusableCell(withIdentifier: "securityTblCell") as! securityTblCell
        let dict = arrPaymentHistoryList[indexPath.section] as! NSDictionary
        cell.lblAmount.text="INR : \((dict["amount"] as? String)!)"
        cell.lblcredit.text=dict["status"] as? String
        
        cell.lblDescription.text="Description : \((dict["description"] as? String)!)"
       
        let dateAsString = "\((dict["date"] as? String)!)"
        let strr : String = ""
        cell.lblDate.text = "Date: \(strr.changeDateFormat(strDate:dateAsString))"
        
        
        let timeAsString = "\((dict["time"] as? String)!)"
        let strTime : String = ""
        let ConvertTime = strTime.changeTimeFormat(strDate: timeAsString)
        cell.lbltime.text="Time : \(ConvertTime)"
        
        return cell
    }
}

class securityTblCell : UITableViewCell{
    
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblcredit : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lbltime : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
}

extension vendorSecurityDepositVC : PGTransactionDelegate{
    
    func setMerchant()
    {
        merchant  = PGMerchantConfiguration.default()!
        merchant.merchantID = "CragFO64001029199264";//paste here your merchant id  //mandatory
        merchant.website = "CragFOWAP";//mandatory
        merchant.industryID = "Retail109";//mandatory
        merchant.channelID = "WAP"; //provided by PG WAP //mandatory
        self.getVendorPaytmCheckSum()
        
    }
    
    func getVendorPaytmCheckSum(){
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["vendor_id" : "\(userid)","amount":"\(txtAddAmount.text!)"] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.getvendor_checksum_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                if (status==true){
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        
                        let callbackurl = responseDict["callback_url"] as? String
                        let checksumHash = responseDict["checksum"] as? String
                        let order_id = responseDict["order_id"] as? String

                        self.merchant  = PGMerchantConfiguration.default()!

                        var odrDict = [String : String]()
                        odrDict["MID"] = "\(self.merchant.merchantID!)"
                        odrDict["WEBSITE"] = "\(self.merchant.website!)"
                        odrDict["INDUSTRY_TYPE_ID"] = "\(self.merchant.industryID!)"
                        odrDict["CHANNEL_ID"] = "\(self.merchant.channelID!)"
                        odrDict["TXN_AMOUNT"] = "\(self.txtAddAmount.text!)"
                        odrDict["ORDER_ID"] = "\(order_id!)"
                        odrDict["CALLBACK_URL"] = "\(callbackurl!)"
                        odrDict["CUST_ID"] = "\(userid)"
                        odrDict["CHECKSUMHASH"] = "\(checksumHash!)"

                        print(odrDict)

                        let order: PGOrder = PGOrder(params: odrDict)
                        let transaction = PGTransactionViewController.init(transactionFor: order)
                        print("transaction == \(transaction!)")
                        transaction!.serverType = eServerTypeProduction
                        transaction!.merchant = self.merchant
                        transaction!.delegate = self
                        self.present(transaction!, animated: true, completion: {

                        })
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                    }
                    
                    //  ApiResponse.alert(title: "", message: strMsg, controller: self)
                    
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
        
    }
    
    
    func randomStringWithLength(len: Int) -> NSString {
        let letters : NSString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 1...len{
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString
    }
    
    
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        print("cancel")
    }
    
    func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        
        print(response)
    }
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        
        print(responseString)
        
//        let str = "{\"ORDERID\":\"162\", \"MID\":\"CragFO64001029199264\", \"TXNID\":\"20180814111212800110168416933582411\", \"TXNAMOUNT\":\"1.00\", \"PAYMENTMODE\":\"PPI\", \"CURRENCY\":\"INR\", \"TXNDATE\":\"2018-08-14 18:40:01.0\", \"STATUS\":\"TXN_SUCCESS\", \"RESPCODE\":\"01\", \"RESPMSG\":\"Txn Success\", \"GATEWAYNAME\":\"WALLET\", \"BANKTXNID\":\"91516965238\", \"BANKNAME\":\"WALLET\", \"CHECKSUMHASH\":\"j3HQeM8D1ZjeSAkFZdXoCQQyFZxIEc8lD8YFIAcsem9ISlsfI3t+hQQbA0FZiQ/ktXcTIvR+5H4lsPq90XiqtzE3mBN0pkKKoHtBwZa8EII=\"}"
        
        let dict = convertToDictionary(text: responseString)
        print(dict!)
        let rescode = dict!["RESPCODE"] as! String
        let status = dict!["STATUS"] as! String
        
        let transID = dict!["TXNID"] as! String
        let orderID = dict!["ORDERID"] as! String
        let txnAmount = dict!["TXNAMOUNT"] as! String
        
        let orderStatus : String!
        if(rescode=="01" && status=="TXN_SUCCESS"){
            orderStatus="true"
        }else{
            orderStatus="false"
        }
        
        self.callVendorDepositApi(strOrderId: orderID, strTransID: transID, strTxnAmount: txnAmount, strStatus: orderStatus)
    }
    
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        print("error--\(error)")
    }
    
    func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        print(response)
    }
    
    
    func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        print(error)
    }
    
    
    
    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        print("response == \(response!)")
    }
    
    
    
}
