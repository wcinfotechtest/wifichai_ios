
import UIKit

class vendorOrderHistryVC: vendorBaseViewController , UISearchBarDelegate {
    
    @IBOutlet weak var tblOrderHistory : UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive : Bool = false
    
    var arrOrderList : [NSDictionary] = []
    var filtered : [NSDictionary] = []
    @IBOutlet weak var lblStatus : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.callOrderHistoryListApi()
        }
        
        
        tblOrderHistory.isHidden=true
        self.lblStatus.isHidden=true
        
        tblOrderHistory.tableFooterView=UIView()
        tblOrderHistory.estimatedRowHeight = 350
        tblOrderHistory.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchBar.text?.isEmpty)!{
            searchActive = false
            tblOrderHistory.reloadData()
        } else {
            
            searchActive = true
            filtered.removeAll()
            
            for i in 0...arrOrderList.count-1{
                
                let dict = arrOrderList[i]
                let vendorDict = dict["customer"] as! NSDictionary
                let tmp = vendorDict["name"] as! NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                
                if(range.location != NSNotFound){
                    filtered.append(dict)
                }else{
                }
            }
        }
        tblOrderHistory.reloadData()
    }
    
    
    func callOrderHistoryListApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        
        ApiResponse.onResponsePost(url: Constant.vendor_history_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        self.arrOrderList = dict["response"] as! [NSDictionary]
                        
                        if(self.arrOrderList.count==0){
                            self.lblStatus.isHidden=false
                            self.tblOrderHistory.isHidden=true
                            
                        }else{
                            self.lblStatus.isHidden=true
                            self.tblOrderHistory.isHidden=false
                            self.tblOrderHistory.reloadData()
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideVendorMenuButtonPressed(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension vendorOrderHistryVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(searchActive) {
            return filtered.count
        }
        
        return arrOrderList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblOrderHistory.dequeueReusableCell(withIdentifier: "vendorOrderTblCell") as! vendorOrderTblCell
        
        var dict : NSDictionary!
        
        
        if(searchActive){
            dict = filtered[indexPath.section]
        }else{
            dict = arrOrderList[indexPath.section]
        }
        
        print(dict)
        if let _ = dict["booking_id"] {
            
            cell.lblCoffeeCount.isHidden = false
            cell.lblBookingId.isHidden = false
            cell.lblVendorEarning.isHidden = true
            cell.contnt_viewHeight.constant=175
            
            
            cell.lblBookingId.text = "  Booking Id : \((dict["booking_id"] as? String)!)"
            cell.lblOrderId.text = "  Order Id : \((dict["order_id"] as? String)!)"
            let vendorDict = dict["customer"] as! NSDictionary
            cell.lblName.text = vendorDict["name"] as? String
            cell.lblNumber.text = vendorDict["mobile"] as? String
            
            let custDict = dict["customer"] as! NSDictionary
            cell.lblAddress.text=custDict["address"] as? String
            
            
            
            
            cell.lblTeaCount.text = "  No of Tea : \((dict["no_of_tea"] as? String)!)"
            cell.lblCoffeeCount.text = "  No of Coffee : \((dict["no_of_coffee"] as? String)!)"
            
            cell.lblTotalAmount.text=""
            cell.lblTotalAmount.isHidden=true
            cell.lblAmount.isHidden=true
            
            
            let dateAsString = "\((dict["date"] as? String)!)"
            let strr : String = ""
            cell.lblDate.text = "  Date: \(strr.changeDateFormat(strDate:dateAsString))"
            
            let timeAsString = "\((dict["time"] as? String)!)"
            let strtime : String = ""
            cell.lblTime.text = "  Time : \(strtime.changeTimeFormat(strDate: timeAsString))"
            
            let status = dict["status"] as! String
            if(status=="1"){
                cell.lblOrderStatus.text = "Initialized"
                cell.lblOrderStatus.textColor = UIColor.green
                
            }else if(status=="2"){
                cell.lblOrderStatus.text = "Pending"
                cell.lblOrderStatus.textColor = UIColor.green
                
            }else if(status=="3"){
                cell.lblOrderStatus.text = "Delivered"
                cell.lblOrderStatus.textColor = UIColor.green
            }
            else if(status=="4"){
                cell.lblOrderStatus.text = "Cancelled"
                cell.lblOrderStatus.textColor = UIColor.red
            }
        }
        else
        {
            cell.lblTotalAmount.isHidden=false
            cell.lblCoffeeCount.isHidden = true
            cell.lblBookingId.isHidden = true
            cell.lblVendorEarning.isHidden = false
            cell.contnt_viewHeight.constant=186
          
            
            
            cell.lblOrderId.text = "  Order Id : \((dict["order_id"] as? String)!)"
            let vendorDict = dict["customer"] as! NSDictionary
            cell.lblName.text = vendorDict["name"] as? String
            cell.lblNumber.text = vendorDict["mobile"] as? String
            let quant = dict["quantity"] as! String
            let amt = dict["product_rate"] as! String
            
            cell.lblAddress.text=dict["address"] as? String
            
            let amount : Int? = Int(amt)
            let quantity : Int? = Int(quant)   //
            let calculateAmt = Int(amount! * quantity!)
            
            let tax = dict["tax"] as! String
            cell.lblAmount.text="  Amount : INR : \(calculateAmt)      Tax:\(tax)%"
            
            let productId = dict["product_id"] as! String
            if(productId=="1"){
                cell.lblTeaCount.text = "  No of Tea : \(quant)"
            }else if(productId=="2"){
                cell.lblTeaCount.text = "  No of Coffee : \(quant)"
            }
            
            cell.lblTotalAmount.text="  Total Amount : INR \((dict["total_amount"] as? String)!)"
            cell.lblVendorEarning.text = "  Vendor Earning : \((dict["vendor_earning"] as? String)!)"
            
            let dateAsString = "\((dict["date"] as? String)!)"
            let strr : String = ""
            cell.lblDate.text = "  Date: \(strr.changeDateFormat(strDate:dateAsString))"
            
            let timeAsString = "\((dict["time"] as? String)!)"
            let strtime : String = ""
            cell.lblTime.text = "  Time : \(strtime.changeTimeFormat(strDate: timeAsString))"
            
            
            let status = dict["status"] as! String
            if(status=="1"){
                cell.lblOrderStatus.text = "Initialized"
                cell.lblOrderStatus.textColor = UIColor.green
                
            }else if(status=="2"){
                cell.lblOrderStatus.text = "Pending"
                cell.lblOrderStatus.textColor = UIColor.green
                
            }else if(status=="3"){
                cell.lblOrderStatus.text = "Delivered"
                cell.lblOrderStatus.textColor = UIColor.green
            }
            else if(status=="4"){
                cell.lblOrderStatus.text = "Cancelled"
                cell.lblOrderStatus.textColor = UIColor.red
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
    }
    
}

class vendorOrderTblCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderId : UILabel!
    @IBOutlet weak var lblBookingId : UILabel!
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblCoffeeCount : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblTotalAmount : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblNumber : UILabel!
    @IBOutlet weak var lblOrderStatus : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblVendorEarning : UILabel!
    
    @IBOutlet weak var contnt_viewHeight : NSLayoutConstraint!
    
    var strUrl : String = ""
    
}



