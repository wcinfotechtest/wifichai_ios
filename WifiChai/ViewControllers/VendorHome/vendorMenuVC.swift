

import UIKit

class vendorMenuVC: UIViewController {
    
    /**
     *  Array to display menu options
     */
    @IBOutlet var tblMenuOptionsVendor : UITableView!
    
    /**
     *  Transparent button to hide menu
     */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptionsVendor.tableFooterView = UIView()
        
        //
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        
     
            arrayMenuOptions.append(["title":"Home", "icon":"Home"])
            arrayMenuOptions.append(["title":"Orders", "icon":"Order"])
            arrayMenuOptions.append(["title":"Corporate Booking", "icon":"notification"])
            arrayMenuOptions.append(["title":"Store", "icon":"profileIcon"])
            arrayMenuOptions.append(["title":"Notification", "icon":"notification"])
            arrayMenuOptions.append(["title":"Profile", "icon":"profileIcon"])
            arrayMenuOptions.append(["title":"Settings", "icon":"setting"])
            arrayMenuOptions.append(["title":"Refer & Earn", "icon":"Refer-&-earn"])
            arrayMenuOptions.append(["title":"Logout", "icon":"Logout"])
       
        
        
        
        tblMenuOptionsVendor.reloadData()
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
    
}
extension vendorMenuVC :  UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuVendorTableCell") as! menuVendorTableCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        
        
        cell.imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        cell.lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}

class menuVendorTableCell : UITableViewCell{
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgIcon : UIImageView!
}
