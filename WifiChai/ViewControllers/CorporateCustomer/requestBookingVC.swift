
import UIKit

class requestBookingVC: UIViewController {
    
    @IBOutlet weak var tblAddSlots : UITableView!
    @IBOutlet weak var tblViewHeight : NSLayoutConstraint!
    
     @IBOutlet weak var txtStartDate : UITextField!
     @IBOutlet weak var txtEndDate : UITextField!
    
    @IBOutlet weak var txtTime : UITextField!
    @IBOutlet weak var txtCoffeeCount : UITextField!
    @IBOutlet weak var txtTeaCount : UITextField!
    
    @IBOutlet weak var lblTotalAmt : UILabel!
    
    
    @IBOutlet weak var lblStartDate : UILabel!
    @IBOutlet weak var lblEndDate : UILabel!
    @IBOutlet weak var lblStartLine : UILabel!
    @IBOutlet weak var lblEndLine : UILabel!
    
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblCoffeeCount : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblTeaLine : UILabel!
    @IBOutlet weak var lblCoffeeLine : UILabel!
    @IBOutlet weak var lblTimeLine : UILabel!
    
    let datepicker = UIDatePicker()
    
    
    var grandTotal : Int = 0
    
    
    var cofeePrice : Int = 0
    var teaPrice : Int = 0
    
    var arrAddSlots : NSMutableArray = []
    
    var isSelectPicker : String = ""
    var isSelectTextfield : String = ""
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate lazy var timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

         self.hideKeyboardWhenTappedAround()
        
      
        
        
        tblViewHeight.constant=0
        
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        let todaysDate = Date()
        datepicker.minimumDate = todaysDate
        
        self.createToolBarOnDatePicker(textfield: txtStartDate)
        self.createToolBarOnDatePicker(textfield: txtEndDate)
        self.createToolBarOnDatePicker(textfield: txtTime)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickOnAddSlotBtn(_ sender : UIButton){
        if(txtTeaCount.text == "" || txtTeaCount.text?.count==0){
            ApiResponse.alert(title: "", message: "This field is required!", controller: self)
        }
        else if(txtCoffeeCount.text == "" || txtCoffeeCount.text?.count==0){
            ApiResponse.alert(title: "", message: "This field is required!", controller: self)
        }
        else if(txtTime.text == "" || txtTime.text?.count==0){
            ApiResponse.alert(title: "", message: "This field is required!", controller: self)
        }
        else{
            
            let dict = ["no_of_tea" : "\(txtTeaCount.text!)","no_of_coffee" : "\(txtCoffeeCount.text!)","time" : "\(txtTime.text!)"]
            self.arrAddSlots.add(dict)
            
            let tQuant = Int(txtTeaCount.text!)
            let cQuant = Int(txtCoffeeCount.text!)
            
            let tea_total = tQuant! * teaPrice
            let coffee_total = cQuant! * cofeePrice
            
            grandTotal = grandTotal + tea_total + coffee_total
            self.lblTotalAmt.text="Total Amount : \(grandTotal)"
            
            self.tblViewHeight.constant =
                (self.tblViewHeight.constant) + 83
            self.tblAddSlots.reloadData()
            
            
            txtCoffeeCount.text=""
            txtTeaCount.text=""
            txtTime.text=""
            
       
        }
        
    }
    
    @IBAction func clickonBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnRequestBookingBtn(_ sender : UIButton){
        
        if(txtStartDate.text == "" || txtStartDate.text?.count==0){
            ApiResponse.alert(title: "", message: "Please enter start date.", controller: self)
        }
        else if(txtEndDate.text == "" || txtEndDate.text?.count==0){
            ApiResponse.alert(title: "", message: "Please enter end date.", controller: self)
        }
        else if(self.arrAddSlots.count==0){
            ApiResponse.alert(title: "", message: "Please add atleast one slot for delivery.", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            showHud()
            let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
            let userid = Userdict["user_id"] as! String
            
            
            
            let param = ["user_id" : "\(userid)","start_date":"\(txtStartDate.text!)","end_date":"\(txtEndDate.text!)","list_of_slots" : self.arrAddSlots] as NSDictionary
            
            ApiResponse.onResponsePost(url: Constant.corporate_booking_request_URL, parms: param) { (dict, error) in
                if(error == ""){
                    let strMsg=dict["message"] as! String
                    let status=dict["status"] as! Bool
                    if (status==true){
                        OperationQueue.main.addOperation {
                            
                            self.hideHUD()
                            
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                                
                                self.dismiss(animated: true, completion: nil)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                            
                            
                            
                        }
                    }
                    else{
                        OperationQueue.main.addOperation {
                            self.hideHUD()
                            ApiResponse.alert(title: "", message: strMsg, controller: self)
                        }
                    }
                }
                else{
                    //LoaderView().hideActivityIndicator()
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            }
            
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
       
    }

}

extension requestBookingVC : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAddSlots.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAddSlots.dequeueReusableCell(withIdentifier: "addSlotTblCell") as! addSlotTblCell
        
        let dict = arrAddSlots[indexPath.section] as! NSDictionary
        cell.lblTeaCount.text="Tea count : \((dict["no_of_tea"] as? String)!)"
        cell.lblCoffeeCount.text="Coffee count : \((dict["no_of_coffee"] as? String)!)"
        cell.lblTime.text="Time : \((dict["time"] as? String)!)"
        
        cell.btnCross.tag=indexPath.section
        cell.btnCross.addTarget(self, action: #selector(clickOnCrossBtn(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func clickOnCrossBtn(_ sender : UIButton){
        
        let tag = sender.tag
        
      
        
        if(arrAddSlots.count>0){
            let dict = arrAddSlots[tag] as! NSDictionary
            let tQuant = Int((dict["no_of_tea"] as? String)!)
            let cQuant = Int((dict["no_of_coffee"] as? String)!)
            
            let tea_total = tQuant! * teaPrice
            let coffee_total = cQuant! * cofeePrice
            
            grandTotal = grandTotal - tea_total - coffee_total
            self.lblTotalAmt.text="Total Amount : \(grandTotal)"
            
            arrAddSlots.removeObject(at: tag)
            self.tblViewHeight.constant =
                (self.tblViewHeight.constant) - 83
            
            self.tblAddSlots.reloadData()
        }
   
        
        
    }
}

class addSlotTblCell: UITableViewCell {
    
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblCoffeeCount : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var btnCross : UIButton!
    
}

extension requestBookingVC : UITextFieldDelegate{
    
    func createToolBarOnDatePicker(textfield : UITextField){
        
        let toolBar = UIToolbar(frame: CGRect(x:0, y:self.view.frame.size.height/6, width:self.view.frame.size.width, height:40.0))
        
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        
        toolBar.barStyle = .blackTranslucent
        
        toolBar.tintColor = UIColor.white
        
        toolBar.backgroundColor = UIColor.black
        
        
        
        //Aligh Left
        let cancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(clickOnCancelBtn(_:)))
        
         let done = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(clickOnDoneBtn(_:)))
        
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        
        
        
        toolBar.setItems([cancel,flexSpace,flexSpace,done], animated: true)
        textfield.inputAccessoryView = toolBar
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
       
        
        if(textField==txtStartDate){
            
            isSelectTextfield="start"
            isSelectPicker="Date"
            
            datepicker.datePickerMode = .date
            txtStartDate.inputView = datepicker
            
            
            
        }else if(textField==txtEndDate){
            
            
            
            isSelectTextfield="end"
            isSelectPicker="Date"
            
            datepicker.datePickerMode = .date
            txtEndDate.inputView = datepicker
        }
        else if(textField==txtTime){
            isSelectPicker="Time"
            datepicker.datePickerMode = .time
            txtTime.inputView = datepicker
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtStartDate){
            lblStartDate.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblStartLine.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
        else if(textField == txtEndDate){
            lblEndDate.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblEndLine.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
        else if(textField == txtTeaCount){
            lblTeaCount.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblTeaLine.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
        else if(textField == txtCoffeeCount){
            lblCoffeeCount.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblCoffeeLine.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
        else if(textField == txtTime){
            lblTime.textColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
            lblTimeLine.backgroundColor = UIColorFromHex(rgbValue: 0xcc1d4e, alpha: 1.0)
        }
      
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if(textField == txtStartDate){
            lblStartDate.textColor = UIColor.gray
            lblStartLine.backgroundColor = UIColor.gray
            
        }
        else if(textField == txtEndDate){
            lblEndDate.textColor = UIColor.gray
            lblEndLine.backgroundColor = UIColor.gray
        }
        else if(textField == txtTeaCount){
            lblTeaCount.textColor = UIColor.gray
            lblTeaLine.backgroundColor = UIColor.gray
        }
        else if(textField == txtCoffeeCount){
            lblCoffeeCount.textColor = UIColor.gray
            lblCoffeeLine.backgroundColor = UIColor.gray
        }
        else if(textField == txtTime){
            lblTime.textColor = UIColor.gray
            lblTimeLine.backgroundColor = UIColor.gray
        }
        
        return true
    }
    
    
    func clickOnDoneBtn(_ sender : UIButton){
        
        if(isSelectPicker=="Date"){
            let strDate = dateFormatter.string(from: datepicker.date)
            self.view.endEditing(true)
        
                if(isSelectTextfield=="start"){
                    txtStartDate.text=strDate
                }
                else if(isSelectTextfield=="end"){
                    txtEndDate.text=strDate
                }
            
        }else if(isSelectPicker=="Time"){

            let strDate = timeFormatter.string(from: datepicker.date)
            txtTime.text=strDate
            self.view.endEditing(true)
        }
        
    }
    
     func clickOnCancelBtn(_ sender : UIButton){
        txtStartDate.resignFirstResponder()
        txtEndDate.resignFirstResponder()
        txtTime.resignFirstResponder()
    }
    
}
