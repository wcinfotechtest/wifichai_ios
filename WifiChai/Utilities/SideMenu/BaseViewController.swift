import UIKit
class BaseViewController: UIViewController, SlideMenuDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let usertype = userDef.value(forKey: "userType") as! String
        if(usertype=="2"){
            switch(index){
            case 0:
                self.openViewControllerBasedOnIdentifier("HomeViewController")
                break
            case 1:
                self.openViewControllerBasedOnIdentifier("OrderListViewController")
                break
            case 2:
                self.openViewControllerBasedOnIdentifier("NotificationListVC")
                break
            case 3:
                self.openViewControllerBasedOnIdentifier("ProfileViewController")
                break
            case 4:
                self.openViewControllerBasedOnIdentifier("SettingViewController")
                break
            case 5:
                
                self.openViewControllerBasedOnIdentifier("ReferandEarnVC")
                break
            case 6:
                
                let alert = UIAlertController(title: "", message: "Are you sure you want to exit?", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                    
                    
                    userDef.set("", forKey: "session")
                    
                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.present(login, animated: true, completion: nil)
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action) in
                }
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
                
                
                break
            default:
                print("default\n", terminator: "")
            }
            
        }
        else if(usertype=="3"){
            switch(index){
            case 0:
                self.openViewControllerBasedOnIdentifier("CorporateCustHomeVC")
                break
            case 1:
                self.openViewControllerBasedOnIdentifier("customerOrderHistryVC")
                break
            case 2:
                self.openViewControllerBasedOnIdentifier("NotificationListVC")
                break
            case 3:
                self.openViewControllerBasedOnIdentifier("ProfileViewController")
                break
            case 4:
                self.openViewControllerBasedOnIdentifier("SettingViewController")
                break
            case 5:
                self.openViewControllerBasedOnIdentifier("ReferandEarnVC")
                break
            case 6:
                let alert = UIAlertController(title: "", message: "Are you sure you want to exit?", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                    userDef.set("", forKey: "session")
                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.present(login, animated: true, completion: nil)
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action) in
                }
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
                break
            default:
                print("default\n", terminator: "")
            }
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(destViewController, animated: false, completion: nil)
       
    }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return defaultMenuImage;
    }
    
     func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10){
            self.slideMenuItemSelectedAtIndex(-1);
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC  = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
}
