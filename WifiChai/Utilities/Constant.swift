
import Foundation

class Constant{
    
    static let Status_Not_200 : String = "statusnot200"
    static let BASE_URL : String = "http://wifi-tea.examine.work/index.php/webservices/"
    static let Login_URL : String = "\(Constant.BASE_URL)login_user"
    static let sendOTP_URL : String = "\(Constant.BASE_URL)forgot_password"
    static let signUp_URL : String = "\(Constant.BASE_URL)signup_user"
    static let verifyOTP_URL : String = "\(Constant.BASE_URL)verify_otp"
    static let chnagePass_URL : String = "\(Constant.BASE_URL)change_password"
    static let getBankDetails_URL : String = "\(Constant.BASE_URL)get_bank_details"
    static let setBankDetails_URL : String = "\(Constant.BASE_URL)set_bank_details"
    static let getCompanyDetails_URL : String = "\(Constant.BASE_URL)get_company_details"
    static let setCompanyDetails_URL : String = "\(Constant.BASE_URL)set_company_details"
    static let document_list_URL : String = "\(Constant.BASE_URL)document_list"
    static let uploadDocument_URL : String = "\(Constant.BASE_URL)uploadDocument"
    static let notification_list_URL : String = "\(Constant.BASE_URL)notification_list"
    static let refer_earn_URL : String = "\(Constant.BASE_URL)refer_earn"
    static let custOrder_History_URL : String = "\(Constant.BASE_URL)customer_history"
    static let updateProfile_URL : String = "\(Constant.BASE_URL)add_profile"
    static let uploadProfileImg_URL : String = "\(Constant.BASE_URL)upload_image"
    static let get_vendor_URL : String = "\(Constant.BASE_URL)get_vendor"
    static let place_order_URL : String = "\(Constant.BASE_URL)place_order"
    static let get_checksum_URL : String = "\(Constant.BASE_URL)get_checksum"
    static let COD_make_payment_URL : String = "\(Constant.BASE_URL)make_payment"
    static let vendor_home_detail_URL : String = "\(Constant.BASE_URL)vendor_home_detail"
    static let get_vendor_storeitem_list_URL : String = "\(Constant.BASE_URL)storeitem_list"
    static let placestore_order_URL : String = "\(Constant.BASE_URL)placestore_order"
    static let storeorder_history_URL : String = "\(Constant.BASE_URL)storeorder_history"
    static let v_Online_Ofline_Status_URL : String = "\(Constant.BASE_URL)vendorOnlineOfline"

    static let vendor_history_URL : String = "\(Constant.BASE_URL)vendor_history"
    static let booking_list_vendor_URL : String = "\(Constant.BASE_URL)booking_list_vendor"
    
    static let order_cancel_vendor_URL : String = "\(Constant.BASE_URL)order_cancel"
    static let order_deliver_vendor_URL : String = "\(Constant.BASE_URL)order_deliver"
    static let vendorpayment_history_URL : String = "\(Constant.BASE_URL)vendorpayment_history"
    static let vendor_wantBooking_URL : String = "\(Constant.BASE_URL)wantBooking"
    static let vendor_booking_acceptdeny_URL : String = "\(Constant.BASE_URL)booking_acceptdeny"
    static let getvendor_checksum_URL : String = "\(Constant.BASE_URL)getvendor_checksum"
    static let vendor_deposite_URL : String = "\(Constant.BASE_URL)vendor_deposite"
    
    
    //Corporate Customer Api
     static let corporate_user_history_URL : String = "\(Constant.BASE_URL)corporate_user_history"
     static let corporate_booking_list_URL : String = "\(Constant.BASE_URL)booking_list"
     static let corporate_booking_request_URL : String = "\(Constant.BASE_URL)booking_request"
     static let user_rating_URL : String = "\(Constant.BASE_URL)user_rating"
     static let add_details_URL : String = "\(Constant.BASE_URL)add_details"
    
}
