

import UIKit

class PdfFileViewController: UIViewController , UIWebViewDelegate{
    
    @IBOutlet weak var webViewPdfView : UIWebView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadFile()
      
      
        // Do any additional setup after loading the view.
    }
    
    func loadFile(){

        let fm = FileManager.default
        let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let myurl = docsurl.appendingPathComponent("downloadedFile.pdf")
        print("myurl---\(myurl)")
        
        OperationQueue.main.addOperation {
            self.webViewPdfView.loadRequest(URLRequest(url: myurl))
        }
        
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        showHud()
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideHUD()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.hideHUD()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
}
