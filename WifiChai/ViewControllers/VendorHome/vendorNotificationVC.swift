

import UIKit

class vendorNotificationVC: vendorBaseViewController {
    
    @IBOutlet weak var tblNotificationList : UITableView!
    var arrNotificationList : [NSDictionary] = []
    @IBOutlet weak var lblStatus : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.callNotificationListApi()
        }
        
        tblNotificationList.isHidden=true
        self.lblStatus.isHidden=true
        
        
        
        
        tblNotificationList.estimatedRowHeight = 110.0
        tblNotificationList.rowHeight = UITableViewAutomaticDimension
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideVendorMenuButtonPressed(sender)
    }
    
    func callNotificationListApi(){
        
        showHud()
        
        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String
        
        let param = ["user_id" : "\(userid)"] as NSDictionary
        print(param)
        
        ApiResponse.onResponsePost(url: Constant.notification_list_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        self.arrNotificationList = dict["response"] as! [NSDictionary]
                        print(self.arrNotificationList)
                        
                        if(self.arrNotificationList.count==0){
                            self.lblStatus.isHidden=false
                            self.tblNotificationList.isHidden=true
                            
                        }else{
                            self.lblStatus.isHidden=true
                            self.tblNotificationList.isHidden=false
                            self.tblNotificationList.reloadData()
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
    
}
extension vendorNotificationVC : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrNotificationList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblNotificationList.dequeueReusableCell(withIdentifier: "vendorNotifyTblCell") as! vendorNotifyTblCell
        let dict = arrNotificationList[indexPath.section]
        cell.lblTitle.text = dict["title"] as? String
        cell.lblDescription.text = dict["description"] as? String
        
        let jsonDict = dict["json_response"] as! NSDictionary
        let time = jsonDict["time"] as? String
        let date = jsonDict["date"] as? String
        
        let strr : String = ""
        let ConvertTime = strr.changeTimeFormat(strDate: time!)
        cell.lblDateTime.text = "\(String(describing: date!)) \(ConvertTime)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
    }
   
}

class vendorNotifyTblCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblDateTime : UILabel!
}

