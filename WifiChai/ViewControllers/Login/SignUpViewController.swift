//
//  SignUpViewController.swift
//  WifiChai
//
//  Created by MacbookPro on 06/06/18.
//  Copyright © 2018 mavencluster. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var lblLogin : UILabel!
    @IBOutlet weak var viewLogin : UIView!
    
    @IBOutlet weak var viewSelect : UIView!
    @IBOutlet weak var btnSelect : UIButton!
    
    
    @IBOutlet weak var viewCustomer : UIView!
    
    @IBOutlet weak var viewPrsnl : UIView!
    @IBOutlet weak var btnSelectCustmr : UIButton!
    
    
    @IBOutlet weak var btnSignUp : UIButton!
    
    @IBOutlet weak var contentView : UIView!
    @IBOutlet var viewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewPrsnlCorporate : UIView!
    
    
    
    
    
    @IBOutlet weak var txtName : UITextField!
    
    
    
    var isUserType : String = ""
    
    
    var isCustomerType : String = ""
    
    @IBOutlet weak var txtCustEmail : UITextField!
    @IBOutlet weak var txtCustMobileNo : UITextField!
    @IBOutlet weak var txtPass : UITextField!
    @IBOutlet weak var txtCnfrmPass : UITextField!
    @IBOutlet weak var txtReferCode : UITextField!
    
    
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblLogin.text = "Login"
        lblLogin.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        lblLogin.frame = CGRect(x: 0, y: 208, width: 50, height: 200)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        viewLogin.addGestureRecognizer(tap)
        viewLogin.isUserInteractionEnabled = true
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        viewHeightConstraint.constant = 80 + contentView.frame.height
        
        viewSelect.isHidden = true
        viewPrsnl.isHidden = true
        btnSelect.setTitle("Customer", for: .normal)
        btnSelectCustmr.setTitle("Personal", for: .normal)
        
        isUserType = "Customer"
        isCustomerType = "2"

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // function which is triggered when handleTap is called
    func handleTap(_ sender: UITapGestureRecognizer) {
        let transition = CATransition()
        transition.duration = 0.7
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let presentedVC = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController")
        present(presentedVC, animated: false, completion: nil)
        
        
        // let nvc = UINavigationController(rootViewController: presentedVC)
        //  nvc.navigationController?.navigationBar.isHidden = true
        
    }
    
    @IBAction func clickOnSelectBtn(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelect.isHidden = false
        })
    }
    
    @IBAction func clickOsnCustomerBtn(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelect.isHidden = true
            self.btnSelect.setTitle("Customer", for: .normal)
            
            self.isUserType = "Customer"
           self.viewPrsnlCorporate.isHidden = false
            
        self.viewCustomer.frame = CGRect(x: 27, y: self.viewPrsnlCorporate.frame.origin.y + self.viewPrsnlCorporate.frame.size.height + 10, width: 249, height: 304)
            
        })
    }
   
    @IBAction func clickOnVenderBtn(_ sender : UIButton){
        
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewSelect.isHidden = true
            self.btnSelect.setTitle("Vendor", for: .normal)
            
            self.isUserType = "Vendor"
            
            self.viewPrsnlCorporate.isHidden = true
            
            self.viewCustomer.frame = CGRect(x: 27, y: self.viewPrsnlCorporate.frame.origin.y + self.viewPrsnlCorporate.frame.size.height - 42, width: 249, height: 304)
        })
    
    }
    
    @IBAction func ClickOnSelectSec(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewPrsnl.isHidden = false
        })
    }
    
    @IBAction func clickOnPrsnBtn(_ sender : UIButton){
        
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewPrsnl.isHidden = true
            self.btnSelectCustmr.setTitle("Personal", for: .normal)
            
            self.isCustomerType = "2"
            
           // self.viewVendor.isHidden = false
           // self.viewCustomer.isHidden = true
        })
    }
    
    @IBAction func clickOnCorporateBtn(_ sender : UIButton){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.viewPrsnl.isHidden = true
            self.btnSelectCustmr.setTitle("Corporate", for: .normal)
            
            self.isCustomerType = "3"
            // self.viewVendor.isHidden = false
            // self.viewCustomer.isHidden = true
        })
    }
    
    
    @IBAction func clickOnSubmitBtn(_ sender : UIButton){
        
        let phoneno = txtCustMobileNo.text?.isPhoneNumber

        if((txtName.text == "") || (txtName.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter your name.", controller: self)
        }

   else if((txtCustMobileNo.text == "") || (txtCustMobileNo.text?.count)==0){
          ApiResponse.alert(title: "Alert", message: "Please enter Mobile number.", controller: self)

        }
        else if((txtPass.text == "") || (txtPass.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Password.", controller: self)
        }
        else if((txtCustEmail.text == "") || (txtCustEmail.text?.count)==0){
            ApiResponse.alert(title: "Alert", message: "Please enter Email address.", controller: self)
        }
        else if(phoneno==false){
            ApiResponse.alert(title: "Alert", message: "Mobile number should be of 10 digit.", controller: self)
        }
        else if(!(ApiResponse.isPwdLenth(password: txtPass.text!))){
            ApiResponse.alert(title: "Alert", message: "Password should be minimum of 7 digit.", controller: self)
        }
        else if(!(ApiResponse.isPwdLenth(password: txtCnfrmPass.text!))){
            ApiResponse.alert(title: "Alert", message: "re-Password should be minimum of 7 digit.", controller: self)
        }
        else if(txtPass.text != txtCnfrmPass.text){
           ApiResponse.alert(title: "Alert", message: "PAssword and re-password are mismatch", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            self.callSignUpApi()
        }
        else{
            ApiResponse.alert(title: "", message: "Please check your internet connection.", controller: self)
        }
        
    }
    
    
    //Mark : Call signup Api
    func callSignUpApi(){
        
        showHud()
        var param: NSDictionary = NSDictionary()
        
        if(self.isUserType == "Customer"){
            param = ["mobile" : "\(txtCustMobileNo.text!)","password" : "\(txtPass.text!)","name" : "\(txtName.text!)","user_type" : "\(isCustomerType)","email":"\(txtCustEmail.text!)","login_type":"1","fcm_token":""] as NSDictionary
        }else if(self.isUserType == "Vendor"){
             param = ["mobile" : "\(txtCustMobileNo.text!)","password" : "\(txtPass.text!)","name" : "\(txtName.text!)","user_type" : "1","email":"\(txtCustEmail.text!)","login_type":"1","fcm_token":""] as NSDictionary
        }
        
        print(param)

        ApiResponse.onResponsePost(url: Constant.signUp_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool
                
                print("dict---\(dict)")
                if (status==true){
                    OperationQueue.main.addOperation {
                        
                        self.hideHUD()
                        
                        let responseDict = dict["response"] as! NSDictionary
                        print(responseDict)
                        userDef.set(responseDict, forKey: "userInfo")
                        
                      
                        
                        let strMobileVerify = responseDict["is_mobile_verified"] as! Bool
                        if(strMobileVerify == false){
                            let alert = UIAlertController(title: "", message: strMsg, preferredStyle: UIAlertControllerStyle.alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in

                                let verify = self.storyboard?.instantiateViewController(withIdentifier: "VerifyUserViewController") as! VerifyUserViewController
                                verify.strMobileNo = self.txtCustMobileNo.text!
                                self.present(verify, animated: true, completion: nil)
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            //Navigate to home screen
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                self.hideHUD()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}

extension SignUpViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        textField.backgroundColor = UIColor.clear
        textField.layer.borderColor = UIColor.white.cgColor
        textField.layer.borderWidth = 3.0
        
        textField.textColor=UIColor.white
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        textField.backgroundColor = UIColor.lightGray
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.borderWidth = 3.0
        
        textField.textColor=UIColor.black
        
        return true
    }
    
}
