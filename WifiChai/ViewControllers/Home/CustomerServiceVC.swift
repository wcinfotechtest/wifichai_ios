//
//  CustomerServiceVC.swift
//  WifiChai
//
//  Created by MacbookPro on 14/06/18.
//  Copyright © 2018 mavencluster. All rights reserved.
//

import UIKit

class CustomerServiceVC: UIViewController , UIWebViewDelegate {
    
    @IBOutlet weak var myWebView : UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        

          self.loadHtmlFile()
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func loadHtmlFile(){
        let url = Bundle.main.url(forResource: "wifi_chai_contact", withExtension: "html")
        print("url----\(url!)")
        
        OperationQueue.main.addOperation {
            let request = URLRequest(url: url!)
           
            self.myWebView.loadRequest(request)
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        showHud()
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
         self.hideHUD()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
         self.hideHUD()
    }
    

  

}
