
import UIKit

class OrderListViewController: BaseViewController , UISearchBarDelegate {
    
    @IBOutlet weak var tblOrderHistory : UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive : Bool = false
    
    var arrOrderList : [NSDictionary] = []
    var filtered : [NSDictionary] = []
    @IBOutlet weak var lblStatus : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.callOrderHistoryListApi()
        }
        
        
        self.hideKeyboardWhenTappedAround()
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        
        tblOrderHistory.isHidden=true
        self.lblStatus.isHidden=true
 
        tblOrderHistory.tableFooterView=UIView()
        tblOrderHistory.estimatedRowHeight = 220
       tblOrderHistory.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchBar.text?.isEmpty)!{
            searchActive = false
            tblOrderHistory.reloadData()
        } else {
            
            searchActive = true
            filtered.removeAll()
            
            for i in 0...arrOrderList.count-1{
                
                let dict = arrOrderList[i]
                let vendorDict = dict["vendor"] as! NSDictionary
                let tmp = vendorDict["name"] as! NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                
                if(range.location != NSNotFound){
                    filtered.append(dict)
                }else{
                }
            }
        }
            tblOrderHistory.reloadData()
        }
        
//        filtered = arrOrderList.filter({ (text) -> Bool in
//
//
//
//            let dict = filtered[0]
//            let vendorDict = dict["vendor"] as! NSDictionary
//            let tmp: NSString = vendorDict["name"] as! NSString
//            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
//            return range.location != NSNotFound
//        })
//        if(filtered.count == 0){
//            searchActive = false;
//        } else {
//            searchActive = true;
//        }
//        self.tblOrderHistory.reloadData()
   // }
    
    func callOrderHistoryListApi(){
        
        showHud()

        let Userdict = userDef.value(forKey: "userInfo") as! NSDictionary
        let userid = Userdict["user_id"] as! String

        let param = ["user_id" : "\(userid)"] as NSDictionary

        ApiResponse.onResponsePost(url: Constant.custOrder_History_URL, parms: param) { (dict, error) in
            if(error == ""){
                let strMsg=dict["message"] as! String
                let status=dict["status"] as! Bool

                if (status==true){
                    OperationQueue.main.addOperation {

                        self.hideHUD()
                        self.arrOrderList = dict["response"] as! [NSDictionary]
                        
                        if(self.arrOrderList.count==0){
                            self.lblStatus.isHidden=false
                            self.tblOrderHistory.isHidden=true

                        }else{
                            self.lblStatus.isHidden=true
                            self.tblOrderHistory.isHidden=false
                            self.tblOrderHistory.reloadData()
                        }
                    }
                }
                else{
                    OperationQueue.main.addOperation {
                        ApiResponse.alert(title: "", message: strMsg, controller: self)
                    }
                }
            }
            else{
                //LoaderView().hideActivityIndicator()
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        }
    }

    @IBAction func clickOnMenuBtn(_ sender : UIButton){
        onSlideMenuButtonPressed(sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension OrderListViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(searchActive) {
            return filtered.count
        }

        return arrOrderList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblOrderHistory.dequeueReusableCell(withIdentifier: "OrderTblCell") as! OrderTblCell
        
        var dict : NSDictionary!
        
        if(searchActive){
            if(filtered.count>0){
                dict = filtered[indexPath.section]
            }
        }else{
            dict = arrOrderList[indexPath.section]
        }
        
        if(dict != nil){
            cell.lblOrderId.text = "  Order Id : \((dict["order_id"] as? String)!)"
            let vendorDict = dict["vendor"] as! NSDictionary
            cell.lblName.text = vendorDict["name"] as? String
            cell.lblAddress.text = vendorDict["address"] as? String
            cell.lblNumber.text = vendorDict["phone"] as? String
            
            let quant = dict["quantity"] as! String
            let amt = dict["product_rate"] as! String
            
            
            
            let amount : Int? = Int(amt)
            let quantity : Int? = Int(quant)   //
            let calculateAmt = Int(amount! * quantity!)
            
            let tax = dict["tax"] as! String
            cell.lblAmount.text="  Amount : INR : \(calculateAmt)      Tax:\(tax)%"
            
            let productId = dict["product_id"] as! String
            if(productId=="1"){
                cell.lblTeaCount.text = "  Tea Count : \(quant)"
            }else if(productId=="2"){
                cell.lblTeaCount.text = "  Coffee Count : \(quant)"
            }
            
            
            cell.btnInvoice.tag = indexPath.section
            
            cell.btnInvoice.addTarget(self, action: #selector(self.clickOnInvoiceBtn(_:)), for: .touchUpInside)
            
            
            
            cell.lblTotalAmount.text="  Total Amount : INR \((dict["total_amount"] as? String)!)"
            cell.lblDeliveryCode.text="Delivery Code : \((dict["pin"] as? String)!)"
            
            let date = (dict["date"] as? String)!
            let strDae : String = ""
            let convertDate = "\(strDae.changeDateFormat(strDate:date))"
            
            let time = (dict["time"] as? String)!
            let strr : String = ""
            let convertTime = "\(strr.changeTimeFormat(strDate: time))"
            
            cell.lblDate.text="  Date : \(convertDate)"
            cell.lblTime.text="Time : \(convertTime)"
            
            let status = dict["status"] as! String
            if(status=="1"){
                cell.lblOrderStatus.text = "Initialized"
                cell.btnInvoice.isHidden=true
                cell.lblDeliveryCode.isHidden=false
                cell.imgInvoice.isHidden=true
                cell.lblOrderStatus.textColor = UIColor.green
                
            }else if(status=="2"){
                cell.lblOrderStatus.text = "Pending"
                cell.btnInvoice.isHidden=true
                cell.imgInvoice.isHidden=true
                cell.lblDeliveryCode.isHidden=false
                
                cell.lblOrderStatus.textColor = UIColor.green
                
            }else if(status=="3"){
                cell.lblOrderStatus.text = "Delivered"
                cell.btnInvoice.isHidden=false
                cell.imgInvoice.isHidden=false
                cell.lblDeliveryCode.isHidden=true
                cell.lblOrderStatus.textColor = UIColor.green
            }
            else if(status=="4"){
                cell.lblOrderStatus.text = "Cancelled"
                cell.lblOrderStatus.textColor = UIColor.red
                
                cell.btnInvoice.isHidden=true
                cell.imgInvoice.isHidden=true
                cell.lblDeliveryCode.isHidden=false
            }
        }
        
       
        
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1.0)
        UIView.animate(withDuration: 0.5, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
    }
    
    @objc func clickOnInvoiceBtn(_ sender : UIButton) {
        
        showHud()
        let dict = arrOrderList[sender.tag]
        let invoiceUrl = dict["invoice_url"] as! String
       
        
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
                let destinationFileUrl = documentsUrl.appendingPathComponent("downloadedFile.pdf")
        
        //Create URL to the source file you want to download
        let fileURL = URL(string: "\(invoiceUrl)")
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL!)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    OperationQueue.main.addOperation {
                        self.hideHUD()
                       
                        let alert = UIAlertController(title: "Alert", message: "Successfully downloaded.", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
                          
                            let web = self.storyboard?.instantiateViewController(withIdentifier: "PdfFileViewController") as! PdfFileViewController
                            self.present(web, animated: true, completion: nil)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    print("already dowlod")
                } catch ( _ ) {
                    
                    
                }
                
            } else {
                print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "");
            }
        }
        task.resume()

    }
    
    
    
   

}

class OrderTblCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderId : UILabel!
    @IBOutlet weak var lblTeaCount : UILabel!
    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblTotalAmount : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblNumber : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblOrderStatus : UILabel!
    
    @IBOutlet weak var lblDeliveryCode : UILabel!
    @IBOutlet weak var btnInvoice : UIButton!
    @IBOutlet weak var imgInvoice : UIImageView!
    
    
    @IBOutlet weak var viewHeightConstraint : NSLayoutConstraint!
    
    var strUrl : String = ""

    
    
}

// as UISearchBar extension
//extension UISearchBar {
//    func changeSearchBarColor(color : UIColor) {
//        for subView in self.subviews {
//            for subSubView in subView.subviews {
//                if subSubView.conformsToProtocol(UITextInputTraits.self) {
//                    let textField = subSubView as UITextField
//                    textField.backgroundColor = color
//                    break
//                }
//            }
//        }
//    }
//}

